<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!doctype html>
<html lang="pt-br">
  <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="generator" content="Jekyll v3.8.5">
    <title>Tela de Seleção - SIAMES</title>

		<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
		<script src="<?php echo base_url();?>js/jquery.js"></script>
         <script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.js"></script>


    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>
  </head>
  <body>
    <header>
  <div class="collapse bg-dark" id="navbarHeader">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 col-md-7 py-4">
          <h4 class="text-white">Sobre</h4>
          <p class="text-muted">SIAMES - <b>Si</b>stema <b>A</b>cadêmico <b>M</b>odular de <b>E</b>nsino <b>S</b>uperior.</p>
        </div>
        <div class="col-sm-4 offset-md-1 py-4">
          <h4 class="text-white">Opções</h4>
          <ul class="list-unstyled">
            <li><a href="<?php echo base_url('login/logout')?>" class="text-white">Sair</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="navbar navbar-dark bg-dark shadow-sm">
    <div class="container d-flex justify-content-between">
      <a href="<?php echo site_url('Selecao'); ?>" class="navbar-brand d-flex align-items-center">
        <strong>SIAMES</strong>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
  </div>
</header>

<main role="main">
  <div class="album py-5 bg-light">
    <div class="container">
      <div class="row">
       <?php foreach($sistemas as $row): ?>
        <div class="col-md-4">
          <div class="card mb-4 shadow-sm">
            <svg class="bd-placeholder-img card-img-top" width="100%" height="225" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: Thumbnail"><title><?=$row['nome']?></title><rect width="100%" height="100%" fill="#55595c"/><text x="50%" y="50%" fill="#eceeef" dy=".3em"><?=$row['nome']?></text></svg>
            <div class="card-body">
              <p class="card-text"><?=$row['descricao']?></p>
              <div class="d-flex justify-content-between align-items-center">
                <div class="btn-group">
                  <a class="btn btn-sm btn-outline-secondary" href="
                  <?php
                  if($row['system_method']!='#'):
                    echo base_url(''.$row["system_method"].'');
                    else:
                      echo '#';
                  endif;?>
                  ">Acessar</a>
                  <button type="button" class="btn btn-sm btn-outline-secondary">Outros</button>
                </div>
                <small class="text-muted">Disponível</small>
              </div>
            </div>
          </div>
        </div>
    <?php endforeach; ?>
      </div>
    </div>
  </div>

</main>

<footer class="text-muted">
</footer>
</body>
</html>
