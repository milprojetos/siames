<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<br>
<div class="container">
<?php echo form_open('os/nova_os'); ?>
<?php foreach($select_usu as $row):?>
<?php foreach($select_ficha as $func):?>
  <div class="form-row">
    <div class="form-group col-md-1">
      <label for="id">ID</label>
      <input type="email" class="form-control-plaintext" id="id" name="id" value="<?=$row['funcionario']?>" readonly>
    </div>
    <div class="form-group col-md-2">
      <label for="data">Data</label>
      <input type="date" class="form-control-plaintext" id="data" name="data" readonly>
    </div>

    <div class="form-group col-md-3">
      <label for="setor_origem">Setor de Origem</label>
      <input type="email" class="form-control-plaintext" id="setor_origem" 
      name="setor_origem" value="<?=$func['setor']?>" readonly="">
    </div>

    <div class="form-group col-md-3">
      <label for="emissor">Emissor</label>
      <input type="email" class="form-control-plaintext" id="emissor" name="emissor" value="<?=$row['nome']?> <?=$row['sobrenome']?>" readonly="">
    </div>

    <div class="form-group col-md-3">
      <label for="funcao">Função</label>
      <input type="email" class="form-control-plaintext" id="funcao" name="funcao" value="<?=$func['funcao']?>" readonly="">
    </div>
  </div>
  <div class="form-row">

    <div class="form-group col-md-6">
      <label for="setor_destino">Setor de Destino</label>
      <select id="setor_destino" name="setor_destino" class="setor_destino form-control" required>
        <option value="">Selecione um setor...</option>
        <?php foreach($select_setor as $setor):?>
        <option value="<?=$setor['id_setor']?>">
        	<?=$setor['descricao']?>
        </option>
		<?php endforeach;?>
      </select>
  </div>
  
  <div class="form-group col-md-6">
      <label for="prioridade">Prioridade</label>
      <select id="prioridade" name="prioridade" class="form-control" required>
        <option value="0" selected>Baixíssima Prioridade</option>
        <option value="1">Baixa Prioridade</option>
        <option value="2">Sem prioridade</option>
        <option value="3">Alta Prioridade</option>
        <option value="4">Urgente</option>
      </select>
  </div>
  </div>
  
      <div class="form-group">
      <label for="assunto">Assunto</label>
      <select id="assunto" name="assunto" class="form-control" required>
        <option value="">Selecione um assunto...</option>
        	<?php foreach($select_assunto as $assunto):?>
        			<option style="display: none;" class="assuntos <?=$assunto['setor']?>" 
        				value="<?=$assunto['id']?>">
        				<?=$assunto['assunto']?>
        			</option>
			<?php endforeach;?>
      </select>
  </div>

    <div class="form-group ">
      <label for="descricao">Descrição</label>
      <textarea id="descricao" name="descricao" class="form-control" rows="7" cols="30" required></textarea>
    </div>
  <button type="submit" class="btn btn-dark">Solicitar</button>
<?php endforeach;?>
<?php endforeach;?>
<?php echo form_close(); ?>
</div>
</body>
</html>