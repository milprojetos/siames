<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
    <title><?php echo $header[0]['nome'];?></title>
    <link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/fa/css/all.css">
    <script src="<?php echo base_url();?>js/jquery.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>js/os.js"></script>
	
	<script type="text/javascript">	var adicionar_os = '<?php echo site_url("os/nova_os");?>';</script>
    <style type="text/css">
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .custom-file-label::after {
            color: #e9ecef;
            content: "Procurar";
            background-color: #28a745;
        }

        .navbar-nav{
            flex-direction: row;
        }

        .nav-item{
            padding-left: 10px;
        }
    </style>
</head>
<body>
<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">Sobre</h4>
                    <p class="text-muted"><?php echo $header[0]['nome'];?> - <?php echo $header[0]['descricao'];?>.</p>
                </div>
            
                <div class="col-sm-4 offset-md-1 py-4">
                    <h4 class="text-white">Opções</h4>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo site_url('Selecao'); ?>" class="text-white">
                                Trocar entre sistemas
                            </a>
                        </li>
                    
                        <li>
                            <a href="<?php echo base_url('login/logout')?>" class="text-white">
                                Sair
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="<?php echo site_url('Selecao'); ?>" class="navbar-brand d-flex align-items-center">
                <strong>SIAMES</strong>™ 
                <small style="padding-left: 10px;"><?php echo $header[0]['nome'];?></small>
            </a>
            <div class="dropdown ml-auto" style="padding-right: 15px;">
              <button class="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-search"></i> Pesquisa
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton" style="width: 320px;">
                <div class="dropdown-item">
                    <form class="form-inline">
                        <div class="input-group input-group-sm mb-3">
                            <div class="input-group-prepend">
                                <input class="form-control mr-sm-2" type="search" placeholder="Pesquisar Assunto" aria-label="Pesquisar">
                            </div>
                            <div class="input-group-append">
                                <button class="btn btn-outline-dark my-2 my-sm-0" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
              </div>
            </div>

            <div class="nav-item active" style="padding-right: 15px;">
                <a class="btn btn-dark" href="notificacoes">
                    <i class="fa fa-info-circle"></i> Notificações
                </a>
            </div>


            <div class="nav-item active" style="padding-right: 15px;">
                <a class="btn btn-dark" href="OS">
                    <i class="fa fa-plus"></i> Nova Comunicação
                </a>
            </div>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>