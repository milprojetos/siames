<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
?>
<main role="main" class="flex-shrink-0">
	<ul class="nav nav-tabs">
		<li class="nav-item">
			<a class="nav-link active" data-toggle="tab" href="#enviado">Solicitações <b>Enviadas</b></a>
		</li>
		
		<li class="nav-item">
			<a class="nav-link" data-toggle="tab" href="#recebido">Solicitações <b>Recebidas</b></a>
		</li>
	</ul>


	<!--Solicitações Enviadas-->
	<div id="conteudo" class="tab-content">
		<div class="tab-pane fade show active" id="enviado">
			<table class="table table-hover table-striped">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Assunto</th>
						<th scope="col">Enviado por</th>
						<th scope="col">Mensagem</th>
						<th scope="col">Data</th>
						<th scope="col">Resposta</th>
						<th scope="col">Status</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach($enviadas as $enviada):
						$bad_date = $enviada['data'];
						$better_date = nice_date($bad_date, 'd/m/Y');
					?>
					<tr>
						<td><?php echo $enviada['assunto'];?></td>
						<td><?php echo $enviada['nome_emissor'];?></td>
						<td><button class="btn btn-outline-dark" data-toggle="modal" 
						data-target="#show_msg_<?php echo $enviada['id'];?>">Ver mensagem</button></td>
						<td><?php echo $better_date;?></td>
						<td><button class="btn btn-outline-dark" data-toggle="modal" 
						data-target="#show_ans_<?php echo $enviada['id'];?>">Ver resposta</button></td>
						<td>Status (enviado, em execução, concluído)</td>
					</tr>
					<!--Modal de Visualizar Mensagem-->
					<div class="modal fade" id="show_msg_<?php echo $enviada['id'];?>" tabindex="-1" 
					role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">
										<?php echo $enviada['assunto'];?>
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<p style= 'color:black;'>Resposta a solicitação <?php echo $enviada['id'];?> no dia <?php echo  $better_date?></p>
									<p style='color:black;'>Dados do solicitador</p>
									<p style='color:black;'>Nome: <?php echo $enviada['nome_emissor'];?></p>
									<p style='color:black;'>Setor: <?php echo $enviada['setor_emissor'];?> - 
									Função: <?php echo $enviada['funcao_emissor'];?></p>
									<p style='color:black;'>Para:  <?php echo $enviada['setor_dest'];?> - Prioridade: 
										<?php echo $enviada['prioridade'];?>
									</p><br>
									<p style='color:black;'>Mensagem:</p>
									<p style='color:black;'><?php echo $enviada['descricao'];?></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">
										Fechar
									</button>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	<!--Solicitações Recebidas-->
		<div class="tab-pane fade" id="recebido">
			<table class="table table-hover table-striped">
				<thead class="thead-dark">
					<tr>
						<th scope="col">Assunto</th>
						<th scope="col">Setor</th>
						<th scope="col">Mensagem</th>
						<th scope="col">Data</th>
						<th scope="col">Respota</th>
						<th scope="col">Marcar como</th>
					</tr>
				</thead>

				<tbody>
					<?php foreach($notificacoes as $notificacao):
						$bad_date = $notificacao['data'];
						$better_date = nice_date($bad_date, 'd/m/Y');
					?>
					<tr>
						<td><?php echo $notificacao['assunto'];?></td>
						<td><?php echo $notificacao['setor_emissor'];?></td>
						<td><button class="btn btn-outline-dark" data-toggle="modal" 
						data-target="#show_msg_return_<?php echo $notificacao['id'];?>">Ver mensagem</button></td>
						<td><?php echo $better_date;?></td>
						
						<td>
							<input type="hidden" class="id_requisicao" value="<?php echo $notificacao['id'];?>">
							<input type="hidden" class='response_date' id="data" name="data">
							<input type="hidden" class="id_solicitador" 
								value="<?php echo $notificacao['id_solicitador'];?>">
							<input type="hidden" class="id_setor_dest" 
								value="<?php echo $notificacao['id_setor_dest'];?>">
							
							<textarea class="msg form-control"></textarea>
							<input type="button" class="env btn btn-success text-white" value="Responder">
							<input type="button" class="btn btn-danger text-white fechar" value="Fechar">
						</td>
						
						<td>
							<button class="btn btn-warning">Em execução</button>
							<button class="btn btn-success">Concluído</button>
						</td>
			
					</tr>
					
					<!--Modal de Visualizar Mensagem-->
					<div class="modal fade" id="show_msg_return_<?php echo $notificacao['id'];?>" tabindex="-1" 
					role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
						<div class="modal-dialog" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="exampleModalLabel">
										<?php echo $notificacao['assunto'];?>
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
										<span aria-hidden="true">&times;</span>
									</button>
								</div>
								<div class="modal-body">
									<p style= 'color:black;'>Solicitação:<?php echo $notificacao['id'];?> - 
									Data: <?php echo  $better_date?></p>
									<p style='color:black;'>Dados do solicitador</p>
									<p style='color:black;'>Nome: <?php echo $notificacao['nome_emissor'];?></p>
									<p style='color:black;'>Setor: <?php echo $notificacao['setor_emissor'];?> - 
									Função: <?php echo $notificacao['funcao_emissor'];?></p>
									<p style='color:black;'>Para:  <?php echo $notificacao['setor_dest'];?> - Prioridade: 
										<?php echo $notificacao['prioridade'];?>
									</p><br>
									<p style='color:black;'>Mensagem:</p>
									<p style='color:black;'><?php echo $notificacao['descricao'];?></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">
										Fechar
									</button>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</tbody>
			</table>
		</div>
</main>