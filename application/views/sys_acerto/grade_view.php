<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
	<div class="container mt-4">
		<fieldset>
			<legend>Grades Curriculares</legend>
			<button class="btn btn-sm text-white bg-primary"  data-toggle="modal" 
			data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar Grade Curricular</button>
			
			<table class="table table-sm table-striped">
				<thead>
					<th>Nome</th>
					<th>Grade Curricular</th>
					<th>Nivel Academico</th>
					<th>Publicação no DOU</th>
					<th>Portaria</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php 
					foreach($grades as $row):
						$nome_curso = '';
					?>
						<?php 
						foreach($cursos as $curso):
							if($curso['id_curso'] == $row['id_curso']){
								$nome_curso = $curso['nome_curso'];
							}
						endforeach;
						$phpdate = strtotime( $row['data_publicacao_dou'] );
						$time = date( 'd/m/Y', $phpdate );
						?>
						<tr>
							<td><?=$row['nome']?></td>
							<td><?=$nome_curso?></td>
							<td><?=$row['nivel_academico']?></td>
							<td><?=$time?></td>
							<td><?=$row['portaria_normativa']?></td>
							<td>
								<button type="button" class="btn btn-warning btn-sm" 
								data-toggle="modal" 
								data-target="#editar<?=$row['id_grade_cur']?>">
									<i class="fas fa-edit"></i> Editar
								</button>

								<button type="button" class="btn btn-danger btn-sm" 
								data-toggle="modal" data-target="#dlt<?=$row['id_grade_cur']?>">
									<i class="fas fa-trash"></i> Excluir
								</button>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	
	<!-- Modal Cadastro -->
	<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" 
	aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog modal-xl" role="document">
			<div class="modal-content p-3">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro"><b>Cadastro de Grade Curricular</b></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<?php echo form_open('grades/add');?> 
					<div class="modal-body">
						<div class="form-group row">
							<label for="nome_grade" class="pr-3 col-form-label">Nome da Grade:</label>
							<input type="text" class="form-control up col-sm-5 nome_grade_cad" name="nome_grade"
							placeholder="Nome da Grade" required>
							
							<label for="nivel" class="pr-3 pl-3 col-form-label">Nivel Acadêmico:</label>
							<input type="text" class="form-control up col-sm-3" name="nivel" 
							placeholder="nivel" required>
						</div>
						
						<div class="form-group row">
							<label for="data_dou" class="pr-3 col-form-label">
								Data de Autorização no DOU:
							</label>
							<input type="date" class="form-control up col-sm-4" name="data_dou" required>
							
							<label for="portaria" class="pr-3 pl-3 col-form-label">Portaria Referente:</label>
							<input type="text" class="form-control up col-sm-3" name="portaria" 
							placeholder="portaria" required>
						</div>
						
						<div class="form-group row">
							<label for="curso" class="col-form-label pr-3">Curso:</label>
							<select class="form-control up col-sm-4" id="curso" name="curso" required>
								<?php foreach($cursos as $row2):?>
									<option value="<?=$row2['id_curso']?>">
										<?=$row2['nome_curso']?> 
									</option>
								<?php endforeach;?>
							</select>
						</div>
						
						<div class="form-group">
							<nav>								
								<div class="float-right"  style="font-size: 0.5rem;">
									<i class="far fa-minus-square fa-3x dlt_sem float-rigth pt-1 pb-1"></i>	
									<i class="far fa-plus-square fa-3x new_sem float-rigth pt-1 pb-1"></i>
								</div>
								
								<div class="nav nav-tabs nav_sem" id="nav-tab" role="tablist"></div>
							</nav>
									
							<div class="tab-content sem_inputs" id="nav-tabContent"></div>
						</div>
					</div>					
					
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" 
						data-dismiss="modal">Fechar</button>
						<button type="submit" class="btn btn-primary">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>