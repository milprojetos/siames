<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
	<div class="container mt-4">
		<fieldset>
			<legend>Disciplinas</legend>
			<button class="btn btn-sm text-white bg-primary"  data-toggle="modal" 
			data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar Aluno</button>
			
			<table class="table table-sm table-striped">
				<thead>
					<th>Nome</th>
					<th>Carga Horaria</th>
					<th>Semestre Indicado</th>
					<th>Tipo de Discipina</th>
					<th>Grade Curricular</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php foreach($disciplinas as $row):?>
						<tr>
							<td><?=$row['nome']?></td>
							<td><?=$row['carga_horaria']?></td>
							<td><?=$row['sem_indicado']?></td>
							<td><?=$row['tipo_disc']?></td>
							<td><?=$row['grade_cur']?></td>
							<td>
								<button type="button" class="btn btn-warning btn-sm" 
								data-toggle="modal" 
								data-target="#editar<?=$row['id_matricula']?>">
									<i class="fas fa-edit"></i> Editar
								</button>

								<button type="button" class="btn btn-danger btn-sm" 
								data-toggle="modal" data-target="#dlt<?=$row['id_matricula']?>">
									<i class="fas fa-trash"></i> Excluir
								</button>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	
	<!-- Modal Cadastro -->
	<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" 
	aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastro de Disciplinas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<?php echo form_open('disciplinas/add');?> 
					<div class="modal-body">
						<div class="form-group">
							<label for="nome">Nome da Disciplina:</label>
							<input type="text" class="form-control up" name="nome" 
							placeholder="Nome da Disciplina" required>
						</div>
						
						<div class="form-group">
							<label for="carga_horaria">Carga Horaria:</label>
							<input type="number" min="0" max = '9999' class="form-control up" 
							name="carga_horaria" placeholder="0" step= '40' required>
						</div>
						
						<div class="form-group">
							<label for="funcao">Grade Curricular:</label>
							<select class="form-control" id="grade" name="grade" 
							placeholder="Selecione uma Grade Curricular" required>
								<?php foreach($grades as $row2):?>
									<option value="<?=$row2['id_grade_cur']?>">
										<?=$row2['nome']?> 
									</option>
								<?php endforeach;?>
							</select>
						</div>
						
						<div class="form-group">
							<label for="funcao">Semestre Indicado:</label>
							<select class="form-control" id="sem_indicado" name="sem_indicado" required>
								<?php foreach($semestres as $row2):?>
									<?php if($row2['id_grade_cur'] == $grade_selecionada): ?>
										<option value="<?=$row2['id_vinculo_as']?>">
											<?=$row2['nome_sem']?> 
										</option>
									<?php endif; ?>
								<?php endforeach;?>
							</select>
						</div>
						
						<div class="form-group">
							<label for="funcao">Tipo de Disciplinas:</label>
							<select class="form-control" id="tipo" name="tipo" required>
								<option value="1">
									Obrigatoria
								</option>
								
								<option value="2">
									Opcional
								</option>
								
								<option value="3">
									Atividades Complementares
								</option>
							</select>
						</div>

						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" 
							data-dismiss="modal">Fechar</button>
							<button type="submit" class="btn btn-primary">Salvar</button>
						</div>
					</div>	
				</form>
			</div>
		</div>
	<!--Fim do Modal-->
</div>