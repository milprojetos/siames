<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	    <script src="<?php echo base_url();?>js/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/login.css">
		<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
      <script type="text/javascript">
        window.setTimeout(function() {
          $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
          });
        }, 2000);
      </script>

      <style type="text/css">
        #mensagem {
            position: absolute;
            left: 28.5%;
            width: 100%;
            z-index: 999;
            width: 600px;
        }
        
        #txt_mensagem {
            margin: 0 auto;
        }
      </style>
</head>
<body>
  <?php if($mensagem!=' '):?>
        <div id="mensagem">
          <div style="padding: 5px;">
            <div id="txt_mensagem" class="alert alert-danger">
               <strong>Erro!</strong> <?=$mensagem;?>
                <button class="close" date-dimsiss="alert">&times;</button>
            </div>
          </div>
        </div>
  <?php endif;?>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
          	<img src="<?php echo base_url();?>img/logoies.jpg">
          	 <hr class="my-4">
             <?php echo validation_errors(); ?>
            <?php echo form_open('login/validacao', 'class="login form-signin" id="form_login"');?>
              <div class="form-label-group">
                <input type="text" id="login" name="login" class="form-control" placeholder="Digite seu nome de usuário" required autofocus>
                <label for="login">Usuário</label>
              </div>

              <div class="form-label-group">
                <input type="password" id="senha" name="senha" class="form-control" placeholder="Digite sua senha" required>
                <label for="senha">Senha</label>
              </div>
              <hr class="my-4">
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Entrar</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>