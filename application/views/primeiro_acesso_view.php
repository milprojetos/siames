<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<title>Bem-vindo</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	    <script src="<?php echo base_url();?>js/jquery.js"></script>
		<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/login.css">
		<script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.js"></script>
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <div class="d-flex justify-content-between">
              <label></label>
              <label>Seja bem-vindo(a) ao <b>SIAMES</b>.</label>
              <label></label>
            </div>
            
            <p>Este é seu primeiro acesso ao sistema e seu login e senha são: <b><?=$senha;?></b>. Para tornar seu acesso mais seguro, modifique a sua senha para poder utilizar nossos sistemas devidamente.</p>

          	 <hr class="my-4">
            <?php echo form_open('login/senha', 'class="login form-signin" id="form_login"');?>
            <input type="hidden" name="senha" id="pass0" value="<?=$senha?>">
              <div class="form-label-group">
                <input type="password" id="pass1" name="senha1" class="form-control" placeholder="Digite seu nome de usuário" required autofocus>
                <label for="pass1">Digite a senha</label>
                <small id="a" style="color: red;display: none">Senha é igual a anterior.</small>
              </div>

              <div class="form-label-group">
                <input type="password" id="pass2" name="senha2" class="form-control" placeholder="Digite sua senha" required>
                <label for="pass2">Digite a senha novamente</label>
                <small id="b" style="color: red;display: none">Senhas não coincidem</small>
              </div>
              
              <hr class="my-4">
              <button class="btn btn-lg btn-primary btn-block text-uppercase" id="save" type="submit">
                alterar e entrar
              </button>
              <a  class="btn btn-lg btn-danger btn-block text-uppercase" 
                  href="logout" type="button" style="color:white">
                sair
              </a>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>

<script type="text/javascript">
  //Validação de senha
  $("#pass1").keyup(function() {
    if ($("#pass0").val() == $("#pass1").val()) {
      $("#a").fadeIn('slow');
      $("#save").prop("disabled",true);
    } else {
      $("#a").fadeOut('slow');
    }
  });

$("#pass2").keyup(function() {
  if ($("#pass1").val() != $("#pass2").val()) {
    $("#b").fadeIn('slow');
    $("#save").prop("disabled",true);
  } else {
    $("#b").fadeOut('slow');
    if(($("#pass0").val() != $("#pass1").val())&&($("#pass0").val() != $("#pass2").val())){
        $("#save").prop("disabled",false);
    }
  }
  });
</script>
</body>
</html>