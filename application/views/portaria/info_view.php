<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link active" data-toggle="tab" href="#ramal">Ramais de Telefone</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#setor">Responsáveis por Setor</a>
  </li>
   <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#coord">Coordenadores</a>
  </li>
</ul>
<div id="conteudo" class="tab-content">
  <div class="tab-pane fade show active" id="ramal">
    <table class="table table-hover table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Número do Ramal</th>
      <th scope="col">Setor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">8002</th>
      <td>Recepção</td>
    </tr>
    <tr>
      <th scope="row">8003</th>
      <td>NAP</td>
    </tr>
    <tr>
      <th scope="row">8004</th>
      <td>NAF</td>
    </tr>
    <tr>
      <th scope="row">8005</th>
      <td>NPPE</td>
    </tr>
    <tr>
      <th scope="row">8006</th>
      <td>Biblioteca</td>
    </tr>
    <tr>
      <th scope="row">8007</th>
      <td></td>
    </tr>
    <tr>
      <th scope="row">8008</th>
      <td>Pessoal/Financeiro</td>
    </tr>
    <tr>
      <th scope="row">8009</th>
      <td></td>
    </tr>
    <tr>
      <th scope="row">8010</th>
      <td>Sala dos Professores</td>
    </tr>
    <tr>
      <th scope="row">8011</th>
      <td></td>
    </tr>
    <tr>
      <th scope="row">8012</th>
      <td></td>
    </tr>
    <tr>
      <th scope="row">8013</th>
      <td>Diretoria</td>
    </tr>
    <tr>
      <th scope="row">8014</th>
      <td>Coordenação</td>
    </tr>
    <tr>
      <th scope="row">8015</th>
      <td>Marketing/Anhanguera</td>
    </tr>
    <tr>
      <th scope="row">8016</th>
      <td>T.I.</td>
    </tr>
  </tbody>
</table>
  </div>
  <div class="tab-pane fade" id="setor">
   <table class="table table-hover table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Responsável</th>
      <th scope="col">Setor</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Flávia / Sâmia / Josafá / Eduardo / Valdecir</th>
      <td>Recepção</td>
    </tr>
    <tr>
      <th scope="row">Flávio</th>
      <td>NAP</td>
    </tr>
    <tr>
      <th scope="row">Samara</th>
      <td>NAF</td>
    </tr>
    <tr>
      <th scope="row">Clóvis</th>
      <td>NPPE</td>
    </tr>
    <tr>
      <th scope="row">Eric</th>
      <td>Biblioteca</td>
    </tr>
    <tr>
      <th scope="row">Ivaneide</th>
      <td>Pessoal/Financeiro</td>
    </tr>
    <tr>
      <th scope="row">Raquel (manhã) / ? (noite)</th>
      <td>Sala dos Professores</td>
    </tr>
    <tr>
      <th scope="row">Mariana</th>
      <td>Diretoria</td>
    </tr>
    <tr>
      <th scope="row">Daiane</th>
      <td>Coordenação</td>
    </tr>
    <tr>
      <th scope="row">Marketing - Nara / Anhanguera - Luís</th>
      <td>Marketing/Anhanguera</td>
    </tr>
    <tr>
      <th scope="row">Karine</th>
      <td>T.I.</td>
    </tr>
  </tbody>
</table>
  </div>
  <div class="tab-pane fade" id="coord">
       <table class="table table-hover table-striped">
  <thead class="thead-dark">
    <tr>
      <th scope="col">Coordenação</th>
      <th scope="col">Curso</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row">Pedagógico</th>
      <td>Flávio</td>
    </tr>
    <tr>
      <th scope="row">Farmácia</th>
      <td>Fábio Filho</td>
    </tr>
    <tr>
      <th scope="row">T.I.</th>
      <td>Pryscilla</td>
    </tr>
    <tr>
      <th scope="row">Turismo</th>
      <td>Bruna Laura</td>
    </tr>
    <tr>
      <th scope="row">Administração</th>
      <td>Naiderson</td>
    </tr>
    <tr>
      <th scope="row">Contábeis</th>
      <td>Ivaneide</td>
    </tr>
  </tbody>
</table>
  </div>
</div>