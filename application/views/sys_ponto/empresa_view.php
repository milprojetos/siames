<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
//Alerta
if($mensagem!=" "):?>
  <div id="mensagem">
    <div style="padding: 5px;">
      <div id="txt_mensagem" class="alert alert-<?=$alert_type?>">
        <?=$mensagem;?>
        <button class="close" date-dimsiss="alert">&times;</button>
      </div>
    </div>
  </div>
<?php endif;?>
<div>
  <div class="container mt-4">
    <fieldset>
      <legend>Empresas</legend>
      <?php if($_SESSION['sys_1']>1): ?> 
      <button class="btn btn-sm text-white bg-primary"  data-toggle="modal" data-target="#cadastrar">
        <i class="fas fa-plus"></i> Cadastrar Empresa
      </button>
      <?php endif;?>
      <table class="table table-sm table-striped">
        <thead>
          <th>ID</th>
          <th>Nome</th>
          <th>Ações</th>
        </thead>
        <tbody>
          <?php foreach($select as $row):?>
          <tr>
            <td><?=$row['id_empresa']?></td>
            <td><?=$row['razao_social']?></td>
            <td>
              <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" 
              data-target="#ver<?=$row['id_empresa']?>">
              <i class="fas fa-eye"></i> Ver mais
            </button>

            <?php if($_SESSION['sys_1']>1): ?> 
              <button type="button" class="btn btn-info btn-sm" data-toggle="modal" 
              data-target="#editar<?=$row['id_empresa']?>">
              <i class="fas fa-edit"></i> Editar
            </button>
            <?php endif;?>

            <?php if($_SESSION['sys_1']==3): ?> 
              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" 
              data-target="#dlt<?=$row['id_empresa']?>">
                <i class="far fa-trash-alt" ></i> Remover
              </button>
            <?php endif;?>

            </td>
          </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </fieldset>
  </div>

  <!-- Modal Cadastro -->
  <div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="cadastro" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastro">Cadastro de Empresas</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Empresa/add','id="add_empresa"');?> 
        <div class="modal-body">
          <div class="form-group">
            <label for="empregador">Empregador ou Razão Social:</label>
            <input type="text" class="form-control up" name="empregador" placeholder="Insira o nome do empregador ou razão social" required>
          </div>
                    <div class="form-group">
            <label for="atividade">Atividade Econômica:</label>
            <input type="text" class="form-control up" name="atividade" placeholder="Digite a atividade Econômica" required>
          </div>
                    <div class="form-group">
            <label for="cnpj">CNPJ:</label>
            <input type="text" class="form-control up" name="cnpj" placeholder="Insira o CNPJ" required>
          </div>
                    <div class="form-group">
            <label for="endereco">Endereço:</label>
            <input type="text" class="form-control up" name="endereco" value="RUA DONA LEOPOLDINA, 912" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <!--Fim do Modal-->
<?php foreach($select as $row):?>
     <!-- Modal Ediçao -->
  <div class="modal fade" id="editar<?=$row['id_empresa']?>" tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="edicao">Editar Empresa</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Empresa/edt','id="edt_empresa"');?>
        <input type="hidden" name="id_empresa" value="<?=$row['id_empresa']?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="empregador">Empregador ou Razão Social:</label>
            <input type="text" class="form-control" name="empregador" value="<?=$row['razao_social']?>" required>
          </div>
                    <div class="form-group">
            <label for="atividade">Atividade Econômica:</label>
            <input type="text" class="form-control" name="atividade" value="<?=$row['atividade_economica']?>" required>
          </div>
                    <div class="form-group">
            <label for="cnpj">CNPJ:</label>
            <input type="text" class="form-control" name="cnpj" value="<?=$row['cnpj']?>" required>
          </div>
                    <div class="form-group">
            <label for="endereco">Endereço:</label>
            <input type="text" class="form-control" name="endereco" value="<?=$row['endereco']?>" required>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary"> Salvar Alterações</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->
     <!-- Modal Ver Mais -->
  <?php foreach($select as $row):?>
  <div class="modal fade" id="ver<?=$row['id_empresa']?>" tabindex="-1" role="dialog" aria-labelledby="nomedaempresa" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="nomedaempresa"><?=$row['razao_social']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
                    <div class="form-group">
            <label for="atividade"><b>Atividade Econômica:</b></label>
            <input type="text" class="form-control-plaintext" id="atividade" value="<?=$row['atividade_economica']?>" readonly>
          </div>
                    <div class="form-group">
            <label for="cnpj"><b>CNPJ:</b></label>
            <input type="text" class="form-control-plaintext" id="cnpj" value="<?=$row['cnpj']?>" readonly>
          </div>
                    <div class="form-group">
            <label for="endereco"><b>Endereço:</b></label>
            <input type="text" class="form-control-plaintext" id="endereco" value="<?=$row['endereco']?>" readonly>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
<?php endforeach; ?>
  <!--Fim do Modal-->
   <!--Modal Excluir-->
    <?php foreach($select as $row):?>
  <div class="modal fade" id="dlt<?=$row['id_empresa']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir <?=$row['razao_social']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Empresa/dlt','id="dlt_empresa"');?>
        <input type="hidden" name="id_empresa" value="<?=$row['id_empresa']?>">
        <h5>Deseja remover permanentemente a empresa <?=$row['razao_social']?>?</h5>
        <small>Essa ação não pode ser desfeita.</small>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-danger">Excluir</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->
</div>

</body>
</html>