<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
//Alerta
if($mensagem!=" "):?>
	<div id="mensagem">
		<div style="padding: 5px;">
			<div id="txt_mensagem" class="alert alert-<?=$alert_type?>">
				<?=$mensagem;?>
				<button class="close" date-dimsiss="alert">&times;</button>
			</div>
		</div>
	</div>
<?php endif;?>

<main role="main" class="flex-shrink-0">
	<div class="container mt-4">
		<fieldset>
			<legend>Funcionários</legend>
			<div class="input-group mb-3">
				<?php if($_SESSION['sys_1']>1): ?>
					<div class="input-group-prepend">
						<button class="btn btn-sm text-white bg-primary" 
						type="button" data-toggle="modal" data-target="#cadastrar">
							<i class="fas fa-user-plus"></i> Cadastrar um Funcionário
						</button>
					</div>
				<?php endif;?>

				<?php if($_SESSION['sys_1']==3): ?>
					<div class="input-group-append">
						<button class="btn btn-sm text-white bg-success" 
						type="button" data-toggle="modal" data-target="#varios">
							<i class="fas fa-users"></i> Cadastrar Vários Funcionários
						</button>
					</div>
				<?php endif;?>
			</div>

			<table class="table table-sm table-striped">
				<thead>
					<th>Nº Ordem</th>
					<th>Nome</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php foreach($select as $row):?>
						<?php if($row['nordem'] == '0'):?>
						<?php else:?>
							<tr>
								<td><?=$row['nordem']?></td>
								<td><?=$row['nome']?> <?=$row['sobrenome']?></td>
								<td>
									<button type="button" class="btn btn-warning btn-sm" 
									data-toggle="modal" data-target="#ver<?=$row['id_funcionario']?>">
										<i class="fas fa-eye"></i> Ver mais
									</button>
	
									<?php if($_SESSION['sys_1']>1): ?>
										<button type="button" class="btn btn-info btn-sm edit_button" 
										data-toggle="modal" data-target="#editar<?=$row['id_funcionario']?>">
											<i class="fas fa-edit"></i> Editar
										</button>
									<?php endif;?>

									<?php if($_SESSION['sys_1']==3): ?>
										<button type="button" class="btn btn-danger btn-sm" 
										data-toggle="modal" data-target="#remover<?=$row['id_funcionario']?>">
											<i class="far fa-trash-alt"></i> Remover
										</button>
									<?php endif;?>
								</td>
							</tr>
						<?php endif;?>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	<!--Fim do conteúdo-->

	<!-- Modal Edição -->
	<?php foreach($select as $row):?>
		<div class="modal fade" id="editar<?=$row['id_funcionario'];?>" 
		tabindex="-1" role="dialog" aria-labelledby="editar" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="editar">Editar Funcionário</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
	
					<?php echo form_open('Funcionario/edt','id="edt_funcionario"');?>
						<input type="hidden" name="id_funcionario" value="<?=$row['id_funcionario']?>">

						<div class="modal-body">
							<div class="form-row">
								<div class="form-group col-md-3">
									<label for="nordem">Nº Ordem:</label>
									<input type="text" class="form-control" name="nordem" 
									value="<?=$row['nordem']?>" required>
								</div>
	
								<div class="form-group col-md-3">
									<label for="empregado">Nome:</label>
									<input type="text" class="form-control" name="empregado" 
									value="<?=$row['nome']?>" required>
								</div>

								<div class="form-group col-md-6">
									<label for="empregado">Sobrenome:</label>
									<input type="text" class="form-control" name="sobrenome" 
									value="<?=$row['sobrenome']?>" required>
								</div>
							</div>

            <div class="form-group">
              <label for="email">E-mail:</label>
              <?php foreach ($email as $row2):?>
                <?php if($row['id_funcionario']==$row2['id_funcionario']):?>
                  <?php if($row2['tipo']==0): ?>
                    <div class="input-group mb-3">
                  	<input  type="email" class="form-control-plaintext down" name="email_primario" 
                            value="<?=$row2['email']?>" readonly=“true”>
                  	<button class="btn btn-light add_email" type="button">
                    	<i class="fas fa-plus" style="font-size: 21px;"></i>
                    </button>
              	  </div>
                  <?php elseif($row2['tipo']==1): ?>
                  <div class="ghost_div">
                  <div class="input-group mb-3">
                  	 <input type="email" class="form-control down email_secundario" name="email[]" 
                            value="<?=$row2['email']?>" required>
                    <div class="input-group-append">
                    	<input type="hidden" class="id_email" value="<?=$row2['id_email']?>">
                      <button type="button" 
                              class="btn btn-outline-danger delete_email">
                        <i class="fas fa-trash-alt"></i>
                      </button>
                    </div>
                  </div>
                  </div>
                  
                  <?php endif; ?>
                <?php endif;?> 
              <?php endforeach;?>
              <div class="emails_bunker"></div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-4">
                <label for="empresa">Nome da empresa:</label>
                <select class="form-control" name="empresa" required>
                <?php foreach($empresa as $row2):?>
                  <option value="<?=$row2['id_empresa']?>"
                    <?php if($row['id_empresa']==$row2['id_empresa']):?>
                      selected
                    <?php endif;?>>
                    <?=$row2['razao_social']?> 
                  </option>
                <?php endforeach;?>
                </select>
              </div>

              <div class="form-group col-md-4">
                <label for="funcao">Função:</label>
                <select class="form-control" id="funcao" name="funcao" required>
                <?php foreach($funcao as $row2):?>
                  <option value="<?=$row2['id_funcao']?>"
                    <?php if($row['id_funcao']==$row2['id_funcao']):?>
                      selected
                    <?php endif;?>>
                    <?=$row2['descricao']?> 
                  </option>
                <?php endforeach;?>
                </select>
              </div>

              <div class="form-group col-md-4">
                <label for="setor">Setor:</label>
                <select class="form-control" id="setor" name="setor" required>
                <?php foreach($setor as $row2):?>  
                  <option value="<?=$row2['id_setor']?>"
                    <?php if($row['id_setor']==$row2['id_setor']):?>
                      selected
                    <?php endif;?>>
                    <?=$row2['descricao']?> 
                  </option>
                <?php endforeach;?>
                </select>
              </div>
            </div>

            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="nctps">Nº CTPS:</label>
                <input type="text" class="form-control" name="nctps" value="<?=$row['num_ctps']?>" required>
              </div>

              <div class="form-group col-md-6">
                <label for="nreg">Nº Reg.:</label>
                <input type="text" class="form-control" name="nreg" value="<?=$row['num_reg']?>"required>
              </div>
            </div>

            <div class="form-group">
              <label for="horario">Horário:</label>
              <input type="text" class="form-control" name="horario" value="<?=$row['horario']?>" required>
            </div>
          </div>
          
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" class="btn btn-primary">Salvar Alterações</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->
  
  <!-- Modal Ver Mais -->
  <?php foreach($select as $row):?>
  <div class="modal fade" id="ver<?=$row['id_funcionario']?>" 
       tabindex="-1" role="dialog" aria-labelledby="vermais" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="vermais"><?=$row['nome']?> <?=$row['sobrenome']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">
          <div class="form-row">
            <div class="form-group col-md-2">
              <label for="nordem">Nº Ordem:</label>
              <input type="text" class="form-control-plaintext" 
                     id="nordem" value="<?=$row['nordem']?>" readonly >
            </div>
            
            <div class="form-group col-md-5">
              <label for="nctps">Nº CTPS:</label>
              <input type="text" class="form-control-plaintext" id="nctps" value="<?=$row['num_ctps']?>" readonly >
            </div>

            <div class="form-group col-md-5">
              <label for="nreg">Nº Reg.:</label>
              <input type="text" class="form-control-plaintext" id="nreg" value="<?=$row['num_reg']?>" readonly >
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col-md-4">
              <label for="empresa">Nome da empresa:</label>
              <?php foreach($empresa as $row2):?>
                <?php if($row['id_empresa']==$row2['id_empresa']):?>
                <input  type="text" class="form-control-plaintext" 
                         id="nreg" value="<?=$row2['razao_social']?>" readonly>
                <?php endif;?>
              <?php endforeach; ?>
            </div>

            <div class="form-group col-md-4">
              <label for="funcao">Função:</label>
              <?php foreach($funcao as $row2):?>
                <?php if($row['id_funcao']==$row2['id_funcao']):?>
                <input  type="text" class="form-control-plaintext" 
                        id="funcao" value="<?=$row2['descricao']?>" readonly>
                <?php endif;?>
              <?php endforeach; ?>
            </div>

            <div class="form-group col-md-4">
              <label for="setor">Setor:</label>
              <?php foreach($setor as $row2):?>
              <?php if($row['id_setor']==$row2['id_setor']):?>
              <input type="text" class="form-control-plaintext" id="setor" value="<?=$row2['descricao']?>" readonly>
              <?php endif;?>
              <?php endforeach; ?>
            </div>
          </div>

          <div class="form-group">
            <label for="horario">Horário:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="horario" value="<?=$row['horario']?>" readonly>
          </div>

          <div class="form-group">
            <label for="email">E-mail:</label>
            <?php foreach ($email as $row2):?>
                <?php if($row['id_funcionario']==$row2['id_funcionario']):?>
                  <input  type="email" class="form-control-plaintext down" name="email" 
                          value="<?=$row2['email']?>" readonly><br>
                <?php endif;
              endforeach;?>
          </div>
        </div>
        
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->

   <!--Modal Excluir-->
  <?php foreach($select as $row):?>
  <div  class="modal fade" id="remover<?=$row['id_funcionario']?>" 
        tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir <?=$row['nome']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">
          <?php echo form_open('Funcionario/dlt','id="dlt_funcionario"');?>
            <input type="hidden" name="id_funcionario" value="<?=$row['id_funcionario']?>">
            <h5>Deseja remover permanentemente o funcionário <?=$row['nome']?>?</h5>
            <small>Essa ação não pode ser desfeita.</small>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
              <button type="submit" class="btn btn-danger">Excluir</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->

	<!-- Modal Cadastro -->
	<div  class="modal fade" id="cadastrar" tabindex="-1" 
	role="dialog" aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastro de Funcionários</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<?php echo form_open('Funcionario/add','id="add_funcionario"');?>
					<div class="modal-body">
						<div class="form-row">
							<div class="form-group col-md-3">
								<?php
									$last_no_order = 0;
									foreach($select as $row){
										if($last_no_order<$row['nordem']){
											$last_no_order=$row['nordem'];
										}
									}
									$last_no_order ++;
								?>
								<label for="nordem">Nº Ordem:</label>
								<input type="number" class="form-control" name="nordem" placeholder="Nº" 
								value=<?= $last_no_order?> required>
							</div>

							<div class="form-group col-md-3">
								<label for="empregado">Nome:</label>
								<input type="text" class="form-control up" name="empregado" 
								placeholder="Nome" required>
							</div>

							<div class="form-group col-md-6">
								<label for="sobrenome">Sobrenome:</label>
								<input type="text" class="form-control up" name="sobrenome" 
								placeholder="Sobrenome" required>
							</div>
						</div>

						<div class="form-group">
							<label for="email">E-mail:</label>
							<input type="email" class="form-control" name="email[]" required>
							
							<div class="emails_inputs"></div>
							<button class="btn new_email" type="button">
								+ Adicionar e-mail secundario
							</button>
						</div>
							
						<div class="form-row">
							<div class="form-group col-md-4">
								<label for="empresa">Empresa:</label>
								<select class="form-control up" name="empresa" required>
									<?php foreach($empresa as $row2):?>
										<option value="<?=$row2['id_empresa']?>">
											<?=$row2['razao_social']?>
										</option>
									<?php endforeach;?>
								</select>
							</div>

							<div class="form-group col-md-4">
								<label for="funcao">Função:</label>
								<select class="form-control" id="funcao" name="funcao" required>
									<?php foreach($funcao as $row2):?>
										<option value="<?=$row2['id_funcao']?>"
											<?php if($row['id_funcao']==$row2['id_funcao']):?>
												selected
											<?php endif;?>
										>
											<?=$row2['descricao']?> 
										</option>
									<?php endforeach;?>
								</select>
							</div>

							<div class="form-group col-md-4">
								<label for="setor">Setor:</label>
								<select class="form-control" id="setor" name="setor" required>
									<?php foreach($setor as $row2):?>
										<option value="<?=$row2['id_setor']?>"
											<?php if($row['id_setor']==$row2['id_setor']):?>
												selected
											<?php endif;?>
										>
											<?=$row2['descricao']?> 
										</option>
									<?php endforeach;?>
								</select>
							</div>
						</div>

						<div class="form-row">
							<div class="form-group col-md-6">
								<label for="nctps">Nº CTPS:</label>
								<input type="text" class="form-control up" name="nctps" 
								placeholder="Carteira de trabalho">
							</div>

							<div class="form-group col-md-6">
								<label for="nreg">Nº Reg.:</label>
								<input type="text" class="form-control up" name="nreg" 
								placeholder="Registro geral">
							</div>
						</div>

						<div class="form-group ">
							<label for="horario">Horário:</label>
							<input type="text" class="form-control up" name="horario" 
							placeholder="Exemplo: H C SEG QUA QUI SEX 8-12/14-18 SAB 8-12" required>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<button type="submit" class="btn btn-primary">Salvar</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	<!--Fim do Modal-->

	<!-- Modal Cadastro -->
	<div  class="modal fade" id="varios" tabindex="-1" 
	role="dialog" aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastrar vários funcionários</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				
				<form action="<?php echo base_url('Funcionario/csv'); ?>" 
				method="post" enctype="multipart/form-data">
					<div class="modal-body">
						<label for="login">
              Para cadastrar vários usuários, você precisa preencher a tabela do arquivo .csv, procurar o arquivo no computador e importar para o sistema. 
              Você pode clicar 
              <a href="<?php echo base_url();?>download/modelo.csv"><b>AQUI</b></a> 
              para baixar o arquivo modelo e também pode clicar em
              <a href="<?php echo site_url()?>Funcionario/manual"><b>MANUAL</b></a> 
              para saber quais ID's adicionar no Setor, função e empresa.
              Clique na mensagem abaixo para procurar o arquivo no seu computador e no botão 
              <b>IMPORTAR</b> para salvar os arquivos.
						</label>
						
						<div class="input-group custom-file">
							<input type="file" class="custom-file-input" id="arquivo" name="csv" required/>
							<label class="custom-file-label" for="arquivo">Importe um arquivo .csv</label>
						</div>
					</div>
					
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
						<input type="submit" class="btn btn-success form-control" name="csvfile" 
						value="Importar">
					</div>
				</form>
			</div>
		</div>
	</div>
</main>