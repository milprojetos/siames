<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div class="container mt-4">
  <fieldset>
    <legend>Dados das Fichas</legend>
    <?php echo form_open('Imprimir/get_dados','id="funcionarios"');?>
      <div class="row">
        <div class="col-md-3">
          <button type="submit" class="btn btn-sm text-white bg-primary">
            <i class="fas fa-print"></i> Imprimir Selecionados
          </button>
        </div>
        
        <div class="col-md-5">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text" id="data">Mês/Ano</span>
            </div>
            <input type="month" id="mes" class="form-control" name="mesano" required>
          </div>
        </div>

        <div class="col-md-4">
          <div class="input-group mb-3">
            <div class="input-group-prepend">
              <span class="input-group-text">Células fantasma</span>
            </div>
            <button type="button" class="btn btn-light btn-outline-dark form-control"
            data-toggle="modal" data-target="#info"
            style="border:1px solid #ced4da;">
              <i class="fas fa-info-circle"></i>
            </button>
            <input  type="number" id="fantasma" class="form-control" name="vazio" 
                    value="0" min="0" max="13">
          </div>
        </div>


      </div>
      <!--Tabela de funcionários-->
      <table class="table table-sm table-striped">
        <thead>
          <th>
            <input type="checkbox" class="marcatudo marcado">
          </th>
          <th>Ordem</th>
          <th>Nome</th>
          <th>Empresa</th>
          <th>Ações</th>
        </thead>
        
        <tbody>
        <?php foreach($select as $row):?>
          <tr>
            <td>
              <input type="checkbox" 
              class="marcado" name="id<?=$row['nordem'];?>" value="<?=$row['nordem'];?>">
            </td>
            <td><?=$row['nordem']?></td>
            <td><?=$row['nome']?> <?=$row['sobrenome']?></td>
            <td><?=$row['razao_social']?></td>
            <td>
              <button type="button" class="btn btn-sm bg-warning"
                      data-toggle="modal" data-target="#ver<?=$row['nordem'];?>">
                <i class="fas fa-eye"></i> Ver
              </button>
            </td>
          </tr>
        <?php endforeach;?>
        </tbody>
      </table>
    </form>
  </fieldset>
</div>
<!--Fim do conteúdo-->

<!-- Modal Ver Mais -->
<?php foreach($select as $row):?>
  <div  class="modal fade" id="ver<?=$row['nordem'];?>" 
        tabindex="-1" role="dialog" aria-labelledby="nomefunc" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="nomefunc"> Dados da Ficha</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <div class="modal-body">
          <div class="form-group">
            <label for="nordem">Nº Ordem:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="nordem" value="<?=$row['nordem'];?>" readonly >
          </div>

          <div class="form-group">
            <label for="atividade">Razão Social:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="atividade" value="<?=$row['razao_social'];?>" readonly>
          </div>

          <div class="form-group">
            <label for="atividade">Atividade Econômica:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="atividade" value="<?=$row['atividade_economica'];?>" readonly>
          </div>

          <div class="form-group">
            <label for="cnpj">CNPJ:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="cnpj" value="<?=$row['cnpj'];?>" readonly>
          </div>
           
          <div class="form-group">
            <label for="endereco">Endereço:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="endereco" value="<?=$row['endereco'];?>" readonly>
          </div>

          <div class="form-group">
            <label for="nctps">Nº CTPS:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="nctps" value="<?=$row['num_ctps'];?>" readonly >
          </div>

          <div class="form-group">
            <label for="empregado">Empregado:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="empregado" value="<?=$row['nome'];?> <?=$row['sobrenome'];?>" readonly >
          </div>
                
          <div class="form-group">
            <label for="nreg">Nº Reg.:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="nreg" value="<?=$row['num_reg'];?>" readonly >
          </div>

          <div class="form-group">
            <label for="funcao">Função:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="funcao" value="<?=$row['funcao'];?>" readonly>
          </div>

          <div class="form-group">
            <label for="horario">Horário:</label>
            <input  type="text" class="form-control-plaintext" 
                    id="horario" value="<?=$row['horario'];?>" readonly>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
        </div>
      </div>
    </div>
  </div>
  <!--Fim do Modal-->
<?php endforeach;?>

<div class="modal fade" id="info" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Células Fantasma</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <span>
          As células fantasmas são células em branco que são adicionadas à impressão.<br>
          Elas servem para ajudar no reaproveitamento de etiquetas não utilizadas.
          Selecione a quantidade de etiquetas que deseja pular.
        </span>
        <br>
      </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>  
        </div>
      </div>
    </div>
  </div>
</div>
</body>
<script src="<?php echo base_url();?>js/sys_ponto.js"></script>
<script type="text/javascript">
  $( "#fantasma" ).change(function() {
          var max = parseInt($(this).attr('max'));
          var min = parseInt($(this).attr('min'));
          if ($(this).val() > max)
          {
              $(this).val(max);
          }
          else if ($(this).val() < min)
          {
              $(this).val(min);
          }       
        }); 
</script>
</html>
