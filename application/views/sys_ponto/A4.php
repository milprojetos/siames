﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<head>
	<title>(<?php echo count($select);?>) 
		ficha<?php if(count($select)>1):echo 's';endif;?></title>
	<style type="text/css">
	.btn{
		margin-bottom: 0;
		font-weight: 400;
		text-align: center;
		white-space: nowrap;
		vertical-align: middle;
		touch-action: manipulation;
		cursor: pointer;
		background-image: none;
		border: 1px solid transparent;
		padding: 6px 12px;
		font-size: 40px;
		line-height: 1.42857143;
		border-radius: 50%;
		-webkit-user-select: none;
		background-color: white;
		color:#FA8072;
	}

	.btn:focus, .btn:hover {
		background-color: #FA8072;
		color:white;
		text-decoration: none;
	}

	body {
		font-size:12px;
		font-family: "Trebuchet MS";
  		background: rgb(204,204,204); 
	}
    
	.espaco{
		padding-left: 0.47cm !important;
		padding-top: 1.52cm !important;
		padding-bottom: 1.52cm !important;
	}

	.espacinho{
		padding-right: 0.20cm !important;
	}

	.ficha{
		text-align: justify;
		max-width: 9.90cm !important;
		max-height: 3.81cm !important;
		min-width: 9.90cm !important;
		min-height: 3.81cm !important;
		display: inline-block;
		word-wrap: break-word;
	}
	.conteudo{
		padding-left: 4px;
		padding-top: 4px;
		margin-top: 0.2cm;
		margin-left: 0.4cm;
	}

	span .minimizar{
		font-size: 90%;
	}

	span .minimizar2{
		font-size: 80%;
	}

	span .minimizar3{
		font-size: 80%;
		letter-spacing: -0.28mm;
	}
	
	page[size="A4"] {
		background: white;
		width: 21cm;
		height: 29.7cm;
		display: block;
		margin: 0 auto;
		margin-bottom: 0.5cm;
	}

	@media print {
		body, page[size="A4"] {
			margin: 0;
			box-shadow: 0;
		}

		*{
			visibility: hidden;
		}

		page[size="A4"], page[size="A4"] *{
			visibility: visible;
		}
	}
</style>


<link rel="stylesheet" href="<?php echo base_url();?>css/fa/css/all.css">
<script src="<?php echo base_url();?>js/jquery.js"></script>
</head>
<body>
<div style="position: absolute; top: 0;right: 0;"><button class="btn imprimir"><i class="fas fa-print"></i></button></div>
<div style="position: absolute; top: 0;left: 0;">
	<a href="../imprimir" class="btn">
		<i class="fas fa-arrow-circle-left"></i>
	</a>
</div>
<page size="A4">
<div class="espaco">
<?php 
$i=0;
//Para cada ficha
foreach ($select as $row): $i++;?>
<?php
//Tamanho de razão social
switch($row['razao_social']){
    case "ANTONIO AUGUSTO TARTUCE ME": $empresa="padrão";$tamanho=""; break;
    case "SERVISILVA - GIVANGLEI QUEIROZ DA SILVA-ME":$empresa='minimizar2" style="font-size: 80%;"';$tamanho="";break;
    case "ASSOCIAÇÃO INTEGRADA DE ENSINO SUPERIOR DO NORDESTE": $empresa='minimizar3" style="font-size: 80%;
            letter-spacing: -0.28mm;"';$tamanho="font-size:80%";break;
}?>
<?php 
if($i==1):
//Para cada célula vazia
for($j=1;$j <= $vazio;$j++):
//Cria uma célula identica à primeira, porém literalmente invisivel
?>
<div class="ficha <?php if($j%2==1):echo 'espacinho'; endif;?>" 	 
	style="visibility: hidden;">
	<div class="conteudo">
	<br>
		<span>SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES</span></br>
		<span>SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES</span></br>
		<span>SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES</span></br>
		<span>SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES</span></br>
		<span>SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES</span></br>
		<span>SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES SIAMES</span>
	</div>
</div>
<?php 
$i++;
endfor;
endif;?>

<div class="ficha <?php if($i%2==1):echo 'espacinho'; endif;?>">
<div class="conteudo">
<br>
<span style="<?=$tamanho;?>">ORDEM: <?=$row['nordem'];?></span>•<span class="<?=$empresa;?>">RAZÃO SOCIAL: <?=$row['razao_social']?></span><br>
<span>ATIV. ECON.: 
<?php if($row['atividade_economica']==" "):?>
------------------------ 
<?php else:?>
<?=$row['atividade_economica'];?>
<?php endif;?> • CNPJ: <?=$row['cnpj'];?></span><br>
<span style="font-size: 80%;">ENDEREÇO: <?=$row['endereco'];?>•CTPS: <?=$row['num_ctps'];?></span></br>
<span>EMPREGADO: <b><?=$row['nome'];?> <?=$row['sobrenome'];?></b></span>   </br>
<span>FUNÇÃO: <?=$row['funcao'];?>•MÊS: <?=$mes;?>•ANO: <?=$ano;?></span>  </br>
<span>HORÁRIO:<span class="minimizar2"><?=$row['horario'];?></span></span>
</div>
</div>
<?php if($i%14==0):?>
</div></page><page size="A4"><div class="espaco">
<?php endif;?>
<?php endforeach;?>
</div>
</page>


<script type="text/javascript">
	$(document).ready(function(){
		$(".imprimir").click(function () {
			window.print();
		}); 
	});

</script>
</body>
</html>