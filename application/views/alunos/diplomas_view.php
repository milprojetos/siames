﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
	<div class="container mt-4">
		<fieldset>
			<legend>Aluno</legend>
			<button class="btn btn-sm text-white bg-primary"  data-toggle="modal" 
			data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar Aluno</button>
			
			<table class="table table-sm table-striped">
				<thead>
					<th>Nome</th>
					<th>CPF Parcial</th>
					<th>Nome Curso</th>
					<th>Data Ingresso</th>
					<th>Data de Conclusão</th>
					<th>Data de Expedição do Diploma</th>
					<th>Data de Registro do Diploma</th>
					<th>Data de Publicação no DOU</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php foreach($diplomas as $row):?>
						<tr>
							<td><?=$row['nome_completo']?></td>
							<td><?=$row['cpf_parcial']?></td>
							<td><?=$row['nome_curso']?></td>
							<td><?=$row['data_ingresso']?></td>
							<td><?=$row['data_conclusao']?></td>
							<td><?=$row['data_exp_diploma']?></td>
							<td><?=$row['data_reg_diploma']?></td>
							<td><?=$row['data_publicacao_dou']?></td>
							<td>
								<button type="button" class="btn btn-warning btn-sm" 
								data-toggle="modal" data-target="#editar<?=$row['id_ies']?>">
									<i class="fas fa-edit"></i> Editar
								</button>

								<button type="button" class="btn btn-danger btn-sm" 
								data-toggle="modal" data-target="#dlt<?=$row['id_ies']?>">
									<i class="fas fa-trash"></i> Excluir
								</button>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	
	<!-- Modal Cadastro -->
	<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" 
	aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastro de Diplomas</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				
				<!--------aqui dentro estarão os dados do form de cadastro-------->
				<?php echo form_open('Funcoes/add');?> 
					<div class="modal-body">
						<div class="form-group">
							<label for="funcao">Função:</label>
							<input type="text" class="form-control up" name="funcao" 
							placeholder="Função" required>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">
							Fechar
						</button>
						<button type="submit" class="btn btn-primary">Salvar</button>
					</div>
				</form>
				<!------------------------Fim do Form------------------------------>
			</div>
		</div>
	</div>
	<!--Fim do Modal-->
</div>