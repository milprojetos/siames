﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
	<div class="container mt-4">
		<fieldset>
			<legend>Cursos</legend>
			<button class="btn btn-sm text-white bg-primary"  data-toggle="modal" 
			data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar Cursos</button>
			
			<table class="table table-sm table-striped">
				<thead>
					<th>ID</th>
					<th>Nome</th>
					<th>EMEC do Curso</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php foreach($cursos as $row):?>
						<tr>
							<td><?=$row['id_curso']?></td>
							<td><?=$row['nome_curso']?></td>
							<td><?=$row['emec_curso']?></td>
							<td>
								<button type="button" class="btn btn-warning btn-sm" 
								data-toggle="modal" data-target="#editar<?=$row['id_curso']?>">
									<i class="fas fa-edit"></i> Editar
								</button>

								<button type="button" class="btn btn-danger btn-sm" 
								data-toggle="modal" data-target="#dlt<?=$row['id_curso']?>">
									<i class="fas fa-trash"></i> Excluir
								</button>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	
	<!-- Modal Cadastro -->
	<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" 
	aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastro de Curso</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<?php echo form_open('Cursos/add');?> 
					<div class="modal-body">
						<div class="form-group">
							<label for="funcao">Nome do Curso:</label>
							<input type="text" class="form-control up" name="nome" 
							placeholder="Nome" required>
						</div>
						
						<div class="form-group">
							<label for="funcao">EMEC do Curso:</label>
							<input type="text" class="form-control up" name="emec" 
							max = '999999' placeholder="EMEC" required>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" 
						data-dismiss="modal">Fechar</button>
						<button type="submit" class="btn btn-primary">Salvar</button>
					</div>
				</form>
			</div>	
		</div>
	</div>
	<!--Fim do Modal-->
	
	<!-- Modal Ediçao -->
	<?php foreach($cursos as $row):?>
		<div class="modal fade" id="editar<?=$row['id_curso']?>"
		tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="edicao">Editar Curso</h5>
						<button type="button" class="close" data-dismiss="modal" 
						aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<?php echo form_open('cursos/edt');?>
						<input type="hidden" name="id_curso" value="<?=$row['id_curso']?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="descricao">Nome do Curso:</label>
								<input  type="text" class="form-control up" name="nome_curso" 
								value="<?=$row['nome_curso']?>" required>
							</div>
							
							<div class="form-group">
								<label for="descricao">EMEC do Curso:</label>
								<input  type="text" class="form-control up" name="emec_curso" 
								max = '999999' value="<?=$row['emec_curso']?>" required>
							</div>
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" 
							data-dismiss="modal">Fechar</button>

							<button type="submit" class="btn btn-primary"> 
							Salvar Alterações</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php endforeach;?>
	<!--Fim do Modal-->

	<!--Modal Excluir-->
	<?php foreach($cursos as $row):?>
		<div class="modal fade" id="dlt<?=$row['id_curso']?>" 
		tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deletar">Excluir <?=$row['nome_curso']?></h5>
						<button type="button" class="close" 
						data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					
					<div class="modal-body">
						<?php echo form_open('cursos/dlt');?>
							<input type="hidden" name="id_curso" value="<?=$row['id_curso']?>">
							Deseja excluir permanentemente o curso de <br>
							<h5><?=$row['nome_curso']?>?</h5>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" 
								data-dismiss="modal">Fechar</button>

								<button type="submit" class="btn btn-danger">Excluir</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach;?>
	<!--Fim do Modal-->
</div>