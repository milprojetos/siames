﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
	<div class="container mt-4">
		<fieldset>
			<legend>IES</legend>
			<button class="btn btn-sm text-white bg-primary"  data-toggle="modal" 
			data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar IES</button>
			
			<table class="table table-sm table-striped">
				<thead>
					<th>ID</th>
					<th>Nome</th>
					<th>EMEC do IES</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php foreach($ies as $row):?>
						<tr>
							<td><?=$row['id_ies']?></td>
							<td><?=$row['nome_ies']?></td>
							<td><?=$row['emec_ies']?></td>
							<td>
								<button type="button" class="btn btn-warning btn-sm" 
								data-toggle="modal" data-target="#editar<?=$row['id_ies']?>">
									<i class="fas fa-edit"></i> Editar
								</button>

								<button type="button" class="btn btn-danger btn-sm" 
								data-toggle="modal" data-target="#dlt<?=$row['id_ies']?>">
									<i class="fas fa-trash"></i> Excluir
								</button>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	
	<!-- Modal Cadastro -->
	<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" 
	aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastro de IES</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<?php echo form_open('Ies/add');?> 
					<div class="modal-body">
						<div class="form-group">
							<label for="funcao">Nome da IES:</label>
							<input type="text" class="form-control up" name="nome" 
							placeholder="Nome" required>
						</div>
						
						<div class="form-group">
							<label for="funcao">EMEC da IES:</label>
							<input type="text" class="form-control up" name="emec" 
							placeholder="EMEC" max = '9999' required>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" 
						data-dismiss="modal">Fechar</button>
						<button type="submit" class="btn btn-primary">Salvar</button>
					</div>
				</form>
			</div>	
		</div>
	</div>
	<!--Fim do Modal-->
	
	<!-- Modal Ediçao -->
	<?php foreach($ies as $row):?>
		<div class="modal fade" id="editar<?=$row['id_ies']?>"
		tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="edicao">Editar ies</h5>
						<button type="button" class="close" data-dismiss="modal" 
						aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<?php echo form_open('ies/edt');?>
						<input type="hidden" name="id_ies" value="<?=$row['id_ies']?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="descricao">Nome do IES:</label>
								<input  type="text" class="form-control up" name="nome_ies" 
								value="<?=$row['nome_ies']?>" required>
							</div>
							
							<div class="form-group">
								<label for="descricao">EMEC do IES:</label>
								<input  type="text" class="form-control up" name="emec_ies" 
								max = '9999' value="<?=$row['emec_ies']?>" required>
							</div>
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" 
							data-dismiss="modal">Fechar</button>

							<button type="submit" class="btn btn-primary"> 
							Salvar Alterações</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php endforeach;?>
	<!--Fim do Modal-->

	<!--Modal Excluir-->
	<?php foreach($ies as $row):?>
		<div class="modal fade" id="dlt<?=$row['id_ies']?>" 
		tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deletar">Excluir <?=$row['nome_ies']?></h5>
						<button type="button" class="close" 
						data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					
					<div class="modal-body">
						<?php echo form_open('ies/dlt');?>
							<input type="hidden" name="id_ies" value="<?=$row['id_ies']?>">
							Deseja excluir permanentemente o ies de <br>
							<h5><?=$row['nome_ies']?>?</h5>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" 
								data-dismiss="modal">Fechar</button>

								<button type="submit" class="btn btn-danger">Excluir</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach;?>
	<!--Fim do Modal-->
</div>