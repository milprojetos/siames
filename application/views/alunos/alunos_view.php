﻿<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
	<div class="container mt-4">
		<fieldset>
			<legend>Aluno</legend>
			<button class="btn btn-sm text-white bg-primary"  data-toggle="modal" 
			data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar Aluno</button>
			
			<table class="table table-sm table-striped">
				<thead>
					<th>RA</th>
					<th>Nome</th>
					<th>CPF Parcial</th>
					<th>Matricula</th>
					<th>Data Ingresso</th>
					<th>Nome Curso</th>
					<th>Ações</th>
				</thead>

				<tbody>
					<?php foreach($alunos as $row):
						$phpdate = strtotime( $row['data_ingresso'] );
						$time = date( 'd/m/Y', $phpdate );
					?>
						<tr>
							<td><?=$row['num_matricula']?></td>
							<td><?=$row['nome_completo']?></td>
							<td><?=$row['cpf_parcial']?></td>
							<td><?=$row['num_matricula']?></td>
							<td><?=$time?></td>
							<td><?=$row['nome_curso']?></td>
							<td>
								<button type="button" class="btn btn-warning btn-sm" 
								data-toggle="modal" 
								data-target="#editar<?=$row['id_matricula']?>">
									<i class="fas fa-edit"></i> Editar
								</button>

								<button type="button" class="btn btn-danger btn-sm" 
								data-toggle="modal" data-target="#dlt<?=$row['id_matricula']?>">
									<i class="fas fa-trash"></i> Excluir
								</button>
							</td>
						</tr>
					<?php endforeach;?>
				</tbody>
			</table>
		</fieldset>
	</div>
	
	<!-- Modal Cadastro -->
	<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" 
	aria-labelledby="cadastro" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="cadastro">Cadastro de Diploma</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<?php echo form_open('aluno/add');?> 
					<div class="modal-body">
						<div class="form-group">
							<label for="funcao">Numero da Matricula:</label>
							<input type="number" max = '99999999999' class="form-control up" 
							name="ra" 
							placeholder="RA" required>
						</div>
					
						<div class="form-group">
							<label for="funcao">Nome:</label>
							<input type="text" class="form-control up" name="nome" 
							placeholder="Nome" required>
						</div>
						
						<div class="form-group">
							<label for="funcao">CPF:</label>
							<input type="number" class="form-control up" name="cpf" 
							placeholder="CPF" max = '99999999999' required>
						</div>
						
						<div class="form-group">
							<label for="funcao">IES Expeditora:</label>
							<select class="form-control" id="ies" name="ies" required>
								<?php foreach($ies as $row2):?>
									<?php if($row2['ativo'] == 1):?>
										<option value="<?=$row2['id_ies']?>">
											<?=$row2['nome_ies']?> 
										</option>
									<?php endif ?>
								<?php endforeach;?>
							</select>
						</div>
						
						<div class="form-group">
							<label for="funcao">Curso:</label>
							<select class="form-control" id="curso" name="curso" required>
								<?php foreach($cursos as $row2):?>
									<?php if($row2['ativo'] == 1):?>
										<option value="<?=$row2['id_curso']?>">
											<?=$row2['nome_curso']?> 
										</option>
									<?php endif ?>
								<?php endforeach;?>
							</select>
						</div>
						
						<div class="form-group">
							<label for="data_in">Data de Ingresso</label>
							<input type="date" class="form-control" 
							id="data_in" name="data_in" required>
						</div>
						
						<div class="form-group">
							<label for="data_out">Data de Conclusão</label>
							<input type="date" class="form-control" 
							id="data_out" name="data_out" required>
						</div>
						
						<div class="form-group">
							<label for="num_exp">Numero de Expedição:</label>
							<input type="number" class="form-control up" name="num_exp" 
							placeholder="Numero de Expedição" max = '99999999999' required>
						</div>
							
						
						<div class="form-group">
							<label for="data_exp_dip">Data Expedição do Diploma</label>
							<input type="date" class="form-control" 
							id="data_exp_dip" name="data_exp_dip" required>
						</div>
						
						<div class="form-group">
							<label for="funcao">IES Registradora:</label>
							<select class="form-control" id="ies_reg" name="ies_reg" required>
								<?php foreach($ies as $row2):?>
									<?php if($row2['ativo'] == 1):?>
										<option value="<?=$row2['id_ies']?>">
											<?=$row2['nome_ies']?> 
										</option>
									<?php endif ?>
								<?php endforeach;?>
							</select>
						</div>
						
						<div class="form-group">
							<label for="data_reg_dip">Data Registro do Diploma</label>
							<input type="date" class="form-control" 
							id="data_reg_dip" name="data_reg_dip" required>
						</div>
						
						<div class="form-group">
							<label for="num_reg">Numero de Registro:</label>
							<input type="number" class="form-control up" name="num_reg" 
							placeholder="Numero de Registro" max = '99999999999' required>
						</div>
						
						<div class="form-group">
							<label for="data_pub">Data Publicação no DOU</label>
							<input type="date" class="form-control" 
							id="data_pub" name="data_pub" required>
						</div>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" 
						data-dismiss="modal">Fechar</button>
						<button type="submit" class="btn btn-primary">Salvar</button>
					</div>
				</form>
			</div>	
		</div>
	</div>
	<!--Fim do Modal-->

	<!-- Modal Ediçao -->
	<?php foreach($alunos as $row):?>
		<div class="modal fade" id="editar<?=$row['id_matricula']?>"
		tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="edicao">Editar Aluno</h5>
						<button type="button" class="close" data-dismiss="modal" 
						aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<?php echo form_open('aluno/edt');?>
						<input type="hidden" name="id_matricula" 
						value="<?=$row['id_matricula']?>">
						<div class="modal-body">
							<div class="form-group">
								<label for="funcao">Numero da Matricula:</label>
								<input type="number" class="form-control up" name="ra" 
								max = '99999999999' value="<?=$row['num_matricula']?>" required>
							</div>
						
							<div class="form-group">
								<label for="funcao">Nome:</label>
								<input type="text" class="form-control up" name="nome" 
								value="<?=$row['nome_completo']?>" required>
							</div>
							
							<div class="form-group">
								<label for="funcao">CPF:</label>
								<input type="number" class="form-control up" name="cpf" 
								max = '99999999999' value="<?=$row['cpf']?>" required>
							</div>
							
							<div class="form-group">
								<label for="funcao">IES Expeditora:</label>
								<select class="form-control" id="ies" name="ies" required>
									<?php foreach($ies as $row2):?>
										<?php if($row2['ativo'] == 1):?>
											<option value="<?=$row2['id_ies']?>"
												<?php if($row['id_exp']==$row2['id_ies']):?>
													selected
												<?php endif;?>>
												<?=$row2['nome_ies']?> 
											</option>
										<?php endif;?>
										
										<?php if($row2['ativo'] == 0):?>
											<?php if($row['id_exp']==$row2['id_ies']):?>
												<option value="<?=$row2['id_ies']?>" selected>
													<?=$row2['nome_ies']?> 
												</option>
											<?php endif;?>
										<?php endif;?>
										
									<?php endforeach;?>
								</select>
							</div>
							
							<div class="form-group">
								<label for="funcao">Curso:</label>
								<select class="form-control" id="curso" name="curso" required>
									<?php foreach($cursos as $row2):?>
										<?php if($row2['ativo'] == 1):?>
											<option value="<?=$row2['id_curso']?>"
												<?php if($row['id_curso']==$row2['id_curso']):?>
													selected
												<?php endif;?>>
												<?=$row2['nome_curso']?> 
											</option>
										<?php endif;?>
										
										<?php if($row2['ativo'] == 0):?>
											<?php if($row['id_curso']==$row2['id_curso']):?>
												<option value="<?=$row2['id_curso']?>" selected>
													<?=$row2['nome_curso']?> 
												</option>
											<?php endif;?>
										<?php endif;?>
										
									<?php endforeach;?>
								</select>
							</div>
							
							<div class="form-group">
								<label for="data_in">Data de Ingresso</label>
								<input type="date" class="form-control" id="data_in" 
								name="data_in" value="<?=$row['data_ingresso']?>" required>
							</div>
							
							<div class="form-group">
								<label for="data_out">Data de Conclusão</label>
								<input type="date" class="form-control" id="data_out" 
								name="data_out" value="<?=$row['data_conclusao']?>" required>
							</div>
							
							<div class="form-group">
								<label for="num_exp">Numero de Expedição:</label>
								<input type="number" class="form-control up" name="num_exp" 
								value="<?=$row['cod_controle_exp']?>" max = '99999999999' required>
							</div>
								
							
							<div class="form-group">
								<label for="data_exp_dip">Data Expedição do Diploma</label>
								<input type="date" class="form-control" id="data_exp_dip"
								name="data_exp_dip" value="<?=$row['data_exp_diploma']?>" required>
							</div>
							
							<div class="form-group">
								<label for="funcao">IES Registradora:</label>
								<select class="form-control" id="ies_reg" name="ies_reg" required>
									<?php foreach($ies as $row2):?>
										<?php if($row2['ativo'] == 1):?>
											<option value="<?=$row2['id_ies']?>"
												<?php if($row['id_reg']==$row2['id_ies']):?>
													selected
												<?php endif;?>>
												<?=$row2['nome_ies']?> 
											</option>
										<?php endif;?>
										
										<?php if($row2['ativo'] == 0):?>
											<?php if($row['id_reg']==$row2['id_ies']):?>
												<option value="<?=$row2['id_ies']?>" selected>
													<?=$row2['nome_ies']?> 
												</option>
											<?php endif;?>
										<?php endif;?>
										
									<?php endforeach;?>
								</select>
							</div>
							
							<div class="form-group">
								<label for="data_reg_dip">Data Registro do Diploma</label>
								<input type="date" class="form-control" id="data_reg_dip"
								name="data_reg_dip" value="<?=$row['data_reg_diploma']?>" required>
							</div>
							
							<div class="form-group">
								<label for="num_reg">Numero de Registro:</label>
								<input type="number" class="form-control up" name="num_reg" 
								max = '99999999999' value="<?=$row['cod_controle_reg']?>" required>
							</div>
							
							<div class="form-group">
								<label for="data_pub">Data Publicação no DOU</label>
								<input type="date" class="form-control" id="data_pub" 
								name="data_pub" value="<?=$row['data_publicacao_dou']?>" required>
							</div>
						</div>
						
						<div class="modal-footer">
							<button type="button" class="btn btn-secondary" 
							data-dismiss="modal">Fechar</button>

							<button type="submit" class="btn btn-primary"> 
							Salvar Alterações</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	<?php endforeach;?>
	<!--Fim do Modal-->
	
	<!--Modal Excluir-->
	<?php foreach($alunos as $row):?>
		<div class="modal fade" id="dlt<?=$row['id_matricula']?>" 
		tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="deletar">
							Excluir <?=$row['nome_completo']?>	
						</h5>
						<button type="button" class="close" 
						data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					
					<div class="modal-body">
						<?php echo form_open('aluno/dlt');?>
							<input type="hidden" name="id_matricula" 
							value="<?=$row['id_matricula']?>">
							Deseja excluir permanentemente o aluno <br>
							<h5><?=$row['nome_completo']?>?</h5>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" 
								data-dismiss="modal">Fechar</button>

								<button type="submit" class="btn btn-danger">Excluir</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach;?>
	<!--Fim do Modal-->
</div>