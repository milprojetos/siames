<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
  <div class="container mt-4">
    <fieldset>
      <legend>Sistemas</legend>
      <?php if($_SESSION['sys_3']>1): ?>
      <button class="btn btn-sm text-white bg-primary"  data-toggle="modal" data-target="#cadastrar">
        <i class="fas fa-plus"></i> Cadastrar Sistema
      </button>
      <?php endif;?>
      <table class="table table-sm table-striped">
        <thead>
          <th>Nome</th>
          <th>Logo</th>
          <th>Descrição</th>
          <?php if($_SESSION['sys_3']>1): ?>
          <th>Caminho</th>
          <?php endif;?>
          <th>Ações</th>
          <th></th>
        </thead>
        <tbody>
          <?php foreach($select as $row):?>
          <tr>
            <td><?=$row['nome']?></td>
            <td><?=$row['logo']?></td>
            <td><?=$row['descricao']?></td>
            <?php if($_SESSION['sys_3']>1): ?>
            <td><?=$row['system_method']?></td>
            <?php endif;?>
            <td>
            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" 
              data-target="#logo<?=$row['id_sistema']?>" style="width: 100%">
              <i class="fas fa-magic"></i> Logo
            </button>
            <?php if($_SESSION['sys_3']>1): ?>
            <button type="button" class="btn btn-info btn-sm" data-toggle="modal" 
              data-target="#editar<?=$row['id_sistema']?>" style="width: 100%">
              <i class="fas fa-edit"></i> Editar
            </button>
            <?php endif;?>
            </td>
            <td>
            <div class="toogle_block">
            	<input type="hidden" class="hidden_id" value = "<?=$row['id_sistema']?>">
            	<?php if($row['ativo']==1):?>
                	<a href="#" style="width: 100%;">
                	<i class="fas fa-toggle-on toogle_sys" style="font-size: 30px;
                	color:green;"></i></a>
              	<?php else:?>
              	<a href="#" style="width: 100%;">
                	<i class="fas fa-toggle-off toogle_sys" style="font-size: 30px;color:green;"></i>
                </a>
            	<?php endif;?>
            </div>
            </td>
          </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </fieldset>
  </div>

  <!-- Modal Cadastro -->
  <div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="cadastro" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastro">Cadastro de Sistema</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form  action="<?php echo base_url('Sistemas/add'); ?>" 
              method="post" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group">
            <label for="nome">Nome do Sistema:</label>
            <input type="text" class="form-control" name="nome" placeholder="Nome mostrado na tela de Seleção" required>
          </div>
                    <div class="form-group">
            <label for="logo">Logo:</label>
            <input type="text" class="form-control" name="logo" placeholder="Logo">
          </div>
                    <div class="form-group">
            <label for="descricao">Descrição:</label>
            <textarea class="form-control" name="descricao" 
                      placeholder="Insira o CNPJ" required>
            </textarea>
          </div>
        <div class="form-group">
          <label for="path">Caminho:</label><br>
          <div class="custom-file">
              <input  type="file" class="custom-file-input" id="path" name="path">
              <label class="custom-file-label">Selecione o arquivo</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <!--Fim do Modal-->

<!-- Modal Ediçao -->
<?php foreach($select as $row):?>
  <div class="modal fade" id="editar<?=$row['id_sistema']?>" tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="edicao">Editar Sistema</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form  action="<?php echo base_url('Sistemas/edt');?>" 
              method="post" enctype="multipart/form-data">
        <input type="hidden" name="id_sistema" value="<?=$row['id_sistema']?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="nome">Nome do Sistema:</label>
            <input  type="text" class="form-control" name="nome" 
                    value="<?=$row['nome']?>" required>
          </div>
                    <div class="form-group">
            <label for="descricao">Descrição:</label>
            <textarea  class="form-control" name="descricao" required><?=$row['descricao']?>
            </textarea>
          </div>
                    
        <div class="form-group">
          <label for="path">Caminho:</label><br>
          <div class="custom-file">
            <input  type="file" class="custom-file-input" name="path">
            <label class="custom-file-label">Atual: <?=$row['system_method']?></label>
          </div>
          </div>
        </div>

        <input type="hidden" name="path_velho" value="<?=$row['system_method']?>">

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary"> Salvar Alterações</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->
</div>

<script>
    $('#path').on('change',function(){
        var arquivo = $(this).val();
        var arquivo2 = arquivo.replace(/\\/g, "-");
        var arquivo3 = arquivo2.split('-');
        var arquivo4 = arquivo3[2].split('.');
        console.log(arquivo4);

        $(this).next('.custom-file-label').html(arquivo4[0]);
    });

      $('#path2').on('change',function(){
        var arquivo = $(this).val();
        var arquivo2 = arquivo.replace(/\\/g, "-");
        var arquivo3 = arquivo2.split('-');
        var arquivo4 = arquivo3[2].split('.');
        console.log(arquivo4);

        $(this).next('.custom-file-label').html(arquivo4[0]);
    });
</script>
</body>
</html>