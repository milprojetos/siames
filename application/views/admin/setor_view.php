<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
  <div class="container mt-4">
    <fieldset>
      <legend>Assuntos</legend>
      <button class="btn btn-sm text-white bg-primary"  data-toggle="modal" data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar Setor</button>
      <table class="table table-sm table-striped">
        <thead>
          <th>Setor</th>
          <th>Responsável</th>
          <th>Ações</th>
        </thead>
        <tbody>
          <?php foreach($select as $row):?>
          <tr>
            <td><?=$row['descricao']?></td>
            <td>
              <?php foreach ($funcionarios as $row2):?>
              <?php if($row['id_responsavel']==$row2['id_funcionario']):?>
                <?=$row2['nome'];?>
                <?php endif;endforeach;?>
            </td>
            <td>
            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" 
              data-target="#editar<?=$row['id_setor']?>">
              <i class="fas fa-edit"></i> Editar
            </button>
            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" 
              data-target="#dlt<?=$row['id_setor']?>">
              <i class="fas fa-trash"></i> Excluir
            </button>
            </td>
          </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </fieldset>
  </div>

  <!-- Modal Cadastro -->
  <div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="cadastro" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastro">Cadastro de Assuntos</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Setores/add');?> 
        <div class="modal-body">
          <div class="form-group">
            <label for="funcao">Setor:</label>
            <input type="text" class="form-control up" name="setor" placeholder="Setor" required>
          </div>

          <div class="form-group">
            <label for="funcao">Responsável:</label>
              <select class="form-control" name="responsavel">
              <?php foreach ($funcionarios as $row2):?>
                <option value="<?=$row2['id_funcionario']?>"> 
                  <?=$row2['nome']?> <?=$row2['sobrenome']?>
                </option>
              <?php endforeach;?>
              </select>
          </div>
        </div>


        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <!--Fim do Modal-->

<!-- Modal Ediçao -->
<?php foreach($select as $row):?>
  <div class="modal fade" id="editar<?=$row['id_setor']?>" tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="edicao">Editar Função</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Setores/edt');?>
        <input type="hidden" name="id_setor" value="<?=$row['id_setor']?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="descricao">Descrição:</label>
            <input  type="text" class="form-control up" name="setor" 
                    value="<?=$row['descricao']?>" required>
          </div>

          <div class="form-group">
            <label for="funcao">Responsável:</label>
              <select class="form-control" name="responsavel">
              <?php foreach ($funcionarios as $row2):?>
                <option value="<?=$row2['id_funcionario']?>"
                  <?php if($row['id_responsavel']==$row2['id_funcionario']):?>
                    selected
                    <?php endif;?>> 
                  <?=$row2['nome']?> <?=$row2['sobrenome']?>
                </option>
              <?php endforeach;?>
              </select>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary"> Salvar Alterações</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->

   <!--Modal Excluir-->
    <?php foreach($select as $row):?>
  <div class="modal fade" id="dlt<?=$row['id_setor']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir <?=$row['descricao']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Setores/dlt');?>
        <input type="hidden" name="id_setor" value="<?=$row['id_setor']?>">
        Deseja remover a função <br><h5><?=$row['descricao']?>?</h5>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-danger">Excluir</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->
</div>

</body>
</html>