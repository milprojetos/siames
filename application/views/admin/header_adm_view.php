<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
	<title><?php echo $header[0]['nome'];?></title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
	<link rel="stylesheet" href="<?php echo base_url();?>bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/sys_ponto/dashboard.css">
    <link rel="stylesheet" href="<?php echo base_url();?>css/fa/css/all.css">
    <script src="<?php echo base_url();?>js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>bootstrap/js/bootstrap.js"></script>
    
    <script type="text/javascript">
		var remove_access = '<?php echo site_url("usuarios/remove_access");?>';
		var insert_access = '<?php echo site_url("usuarios/insert_access");?>';
		var update_access = '<?php echo site_url("usuarios/edit_access");?>';
		var toogle_sys = '<?php echo site_url("sistemas/toogle_sys");?>';
   </script>
   <script src="<?php echo base_url();?>js/usuarios_view.js"></script>
   <script src="<?php echo base_url();?>js/sistemas_view.js"></script>
   <script type="text/javascript">
    var result =location.pathname.split('/');
    var path = result[2];
    $(document).ready(function(){
        $('.navbar').find('a[href="'+path+'"]').parents('li').addClass('active');    
    });

    window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
          });
        }, 2000);
    
   </script>
     <style type="text/css">
        .up{
            text-transform: uppercase;
        }
        footer{
            background-color:#c7c7c7;
            width: 100%;
        }
        .custom-file-label::after {
            color: #e9ecef;
            content: "Procurar";
            background-color: #28a745;
        }

        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }

        .custom-file-label::after {
            color: #e9ecef;
            content: "Procurar";
            background-color: #28a745;
        }

        .navbar-nav{
            flex-direction: row;
        }

        .nav-item{
            padding-left: 10px;
        }

        #mensagem {
            position: absolute;
            top:0;
            left: 28.5%;
            width: 100%;
            z-index: 999;
            width: 600px;
        }
        
        #txt_mensagem {
            margin: 0 auto;
        }
    </style>
</head>
<body>
<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">Sobre</h4>
                    <p class="text-muted"><?php echo $header[0]['nome'];?> - <?php echo $header[0]['descricao'];?>.</p>
                </div>
            
                <div class="col-sm-4 offset-md-1 py-4">
                    <h4 class="text-white">Opções</h4>
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?php echo site_url('Selecao'); ?>" class="text-white">
                                Trocar entre sistemas
                            </a>
                        </li>
                    
                        <li>
                            <a href="<?php echo base_url('login/logout')?>" class="text-white">
                                Sair
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    
    <div class="navbar navbar-dark bg-dark shadow-sm">
        <div class="container d-flex justify-content-between">
            <a href="<?php echo site_url('Selecao'); ?>" class="navbar-brand d-flex align-items-center">
                <strong>SIAMES</strong>™ 
                <small style="padding-left: 10px;"><?php echo $header[0]['nome'];?></small>
            </a>
            <ul class="navbar-nav ml-auto">
                <!--Itens do Menu-->
                <li class="nav-item">
                    <a class="nav-link" href="usuarios">
                        <i class="fa fa-users-cog"></i> Acessos
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="sistemas">
                        <i class="fa fa-project-diagram"></i> Sistemas
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="assuntos">
                        <i class="fa fa-comments"></i> Assuntos
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="funcoes">
                        <i class="fa fa-people-carry"></i> Funções
                    </a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="setores">
                        <i class="fa fa-vector-square"></i> Setores
                    </a>
                </li>
                
                <li class="nav-item">
                    <a class="nav-link" href="lixeira">
                        <i class="fa fa-trash"></i> Lixeira
                    </a>
                </li>
            
            </ul>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation" style="margin-left: 10px">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>