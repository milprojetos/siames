<div>
  <div class="container mt-4">
    <fieldset>
      <legend>Assuntos</legend>
      <button class="btn btn-sm text-white bg-primary"  data-toggle="modal" data-target="#cadastrar"><i class="fas fa-plus"></i> Cadastrar assunto</button>
      <table class="table table-sm table-striped">
        <thead>
          <th>Assunto</th>
          <th>Descrição</th>
          <th>Ações</th>
        </thead>
        <tbody>
          <?php foreach($select as $row):?>
          <tr>
            <td><?=$row['assunto']?></td>
            <td><?=$row['descricao']?></td>
            <td>
            <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" 
              data-target="#editar<?=$row['id_assunto']?>">
              <i class="fas fa-edit"></i> Editar
            </button>
            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" 
              data-target="#dlt<?=$row['id_assunto']?>">
              <i class="fas fa-trash"></i> Excluir
            </button>
            </td>
          </tr>
          <?php endforeach;?>
        </tbody>
      </table>
    </fieldset>
  </div>

  <!-- Modal Cadastro -->
  <div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="cadastro" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastro">Cadastro de Assuntos</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Assuntos/add');?> 
        <div class="modal-body">
          <div class="form-group">
            <label for="assunto">Assunto:</label>
            <input type="text" class="form-control" name="assunto" placeholder="Assunto" required>
          </div>

          <div class="form-group">
            <label for="descricao">Descrição:</label>
            <textarea class="form-control" name="descricao" required></textarea>
          </div>

          <div class="form-group">
            <label for="setor">Setor:</label>
            <?php foreach($setor as $row2):?>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="setor[]" class="setor" value="<?=$row2['id_setor']?>" id="setor_<?=$row2['id_setor']?>">
                </div>
              </div>
              <span class="form-control" for="setor_<?=$row2['id_setor']?>">
                <?=$row2['descricao']?>
              </span>
            </div>
            <?php endforeach;?>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <!--Fim do Modal-->

<!-- Modal Ediçao -->
<?php foreach($select as $row):?>
  <div class="modal fade" id="editar<?=$row['id_assunto']?>" tabindex="-1" role="dialog" aria-labelledby="edicao" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="edicao">Editar Assunto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Assuntos/edt');?>
        <input type="hidden" name="id_assunto" value="<?=$row['id_assunto']?>">
        <div class="modal-body">
          <div class="form-group">
            <label for="assunto">Assunto:</label>
            <input  type="text" class="form-control" name="assunto" 
                    value="<?=$row['assunto']?>" required>
          </div>
          
          <div class="form-group">
            <label for="descricao">Descrição:</label>
            <textarea  class="form-control" name="descricao" required><?=$row['descricao']?>
            </textarea>
          </div>

          <div class="form-group">
            <label for="setor">Setor:</label>
            <?php foreach($setor as $row2):?>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                    <input type="checkbox" name="setor[]" class="setor" value="<?=$row2['id_setor']?>" id="setor_<?=$row2['id_setor']?>"

                  <?php foreach($as as $row3):?>
                    <?php if (($row['id_assunto']==$row3['id_assunto'])&&($row2['id_setor']==$row3['id_setor'])): ?>
                      checked
                    <?php endif ?>
                  <?php endforeach;?>

                    >
                </div>
              </div>
              <span class="form-control" for="setor_<?=$row2['id_setor']?>">
                <?=$row2['descricao']?>
              </span>
            </div>
            <?php endforeach;?>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary"> Salvar Alterações</button>
        </div>
      </form>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->

   <!--Modal Excluir-->
    <?php foreach($select as $row):?>
  <div class="modal fade" id="dlt<?=$row['id_assunto']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir <?=$row['assunto']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Assuntos/dlt');?>
        <input type="hidden" name="id_assunto" value="<?=$row['id_assunto']?>">
        Deseja remover o assunto <br><h5><?=$row['assunto']?>?</h5>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-danger">Excluir</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->
</div>

</body>
</html>