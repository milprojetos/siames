<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
	$nivel1='';
	$nivel2='';
	$nivel3='';
?>
<main role="main" class="flex-shrink-0">
<div>
  <div class="container mt-4">
    <fieldset>
      <legend>Usuários</legend>

      <div class="input-group mb-3">
        <div class="input-group-prepend">
          <button class="btn btn-sm text-white bg-primary" type="button" data-toggle="modal" data-target="#cadastrar"><i class="fas fa-user-plus"></i> Cadastrar um usuário</button>
        </div>
        <div class="input-group-append">
          <button class="btn btn-sm text-white bg-success" type="button" data-toggle="modal" data-target="#varios"><i class="fas fa-users"></i> Cadastrar Vários Usuários</button>
        </div>
      </div>
      <table class="table table-sm table-striped">
        <thead>
          <th>Login</th>
          <th>Nome</th>
          <!--<th>Nível</th>-->
          <th>Linkado ao funcionário</th>
          <th>Opções</th>
        </thead>
        <tbody>
          <?php foreach($usuarios as $row):?>
          <?php
          // O Desenvolvedor pode ver e alterar seu próprio Login, mas o Administrador não 
          if($row['id']==$_SESSION['id']):
          if($_SESSION['sys_3']>1):?>
          <tr>
            <td><?=$row['login']?></td>
            <td><?=$row['apelido']?></td>
            <td><?=$row['nome']?> <?=$row['sobrenome']?></td>
            <td>
              <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#senha<?=$row['id']?>"><i class="fas fa-key"></i> Trocar senha</button>
              <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#acesso<?=$row['id']?>"><i class="fas fa-door-open"></i> Acesso</button>
              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#remover<?=$row['id']?>"><i class="far fa-trash-alt"></i> Remover</button>
            </td>
          </tr>
          <?php 
          endif;
          else:
          if($row['id']!=$_SESSION['id']):?>
          <tr>
            <td><?=$row['login']?></td>
            <td><?=$row['apelido']?></td>
            <td><?=$row['nome']?> <?=$row['sobrenome']?></td>
            <td>
              <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#senha<?=$row['id']?>"><i class="fas fa-key"></i> Trocar senha</button>
              <button type="button" class="btn btn-info btn-sm" data-toggle="modal" data-target="#acesso<?=$row['id']?>"><i class="fas fa-door-open"></i> Acesso</button>
              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#remover<?=$row['id']?>"><i class="far fa-trash-alt"></i> Remover</button>
            </td>
          </tr>
          <?php endif;?>
          <?php endif;?>
          <?php endforeach;?>
        </tbody>
      </table>
    </fieldset>
  </div>
<!--Modal Cadastro-->
<div class="modal fade" id="cadastrar" tabindex="-1" role="dialog" aria-labelledby="cadastro" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastro">Cadastro de Usuário</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <?php echo form_open('Usuarios/add_usuario','id="add"');?>
        <div class="modal-body">
          <div class="form-group">
            <label for="nome">Nome no sistema:</label>
            <input type="text" class="form-control" name="nome" placeholder="O nome do sistema difere do nome de funcionário." required>
          </div>
          <div class="form-group">
            <label for="login">Login:</label>
            <input type="text" class="form-control" name="login" placeholder="Login" required>
          </div>

          <div class="form-group">
            <label for="sobrenome">Senha:</label>
            <input type="password" class="form-control" name="senha" placeholder="Senha" required>
          </div>

          <div class="form-group">
            <label for="empresa">Relacionado ao funcionário:</label>
            <select class="form-control" name="funcionario" required>
            <option value="">Selecione o funcionário...</option>
            <?php foreach($funcionarios as $row2):?>
            <option value="<?=$row2['id']?>">
              <?=$row2['nome']?> <?=$row2['sobrenome']?>
            </option>
            <?php endforeach;?>
            </select>
          </div>

          <div class="form-group">
            <label for="sistema">Garantir acesso a:</label>
            <?php foreach($sistema as $row2):?>
            <div class="input-group mb-3">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="sistema[]" class="sistema" value="<?=$row2['id_sistema']?>" id="sistema_<?=$row2['id_sistema']?>">
                </div>
              </div>
              <span class="form-control" for="sistema_<?=$row2['id_sistema']?>">
                <?=$row2['nome']?>
              </span>
              <select class="custom-select nivel" name="nivel[]">
                <option value="">Nível de acesso...</option>
                <option value="1">Somente Leitura</option>
                <option value="2">Editor</option>
                <option value="3">Administrador</option>
              </select>
            </div>
            <?php endforeach;?>
          </div>

        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-primary">Salvar</button>
        </div>
          </form>
      </div>
    </div>
  </div>
</div>
  <!--Fim do Modal-->

  <!-- Modal mudar senha-->
  <?php foreach($usuarios as $row):?>
  <div  class="modal fade" id="senha<?=$row['id'];?>" 
        tabindex="-1" role="dialog" aria-labelledby="editar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="editar">Editar Senha de <b><?=$row['login'];?></b></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        
        <?php echo form_open('Usuarios/senha','id="senha"');?>
          <input type="hidden"  name="id" value="<?=$row['id']?>">
          <input type="hidden" id="pass0" name="senha_antiga" value="<?=$row['senha']?>">
          <div class="modal-body">
            <div class="form-group">
              <label for="pass1">Nova senha:</label>
              <input type="password" id="pass1" class="form-control" name="senha" required>
              <small id="a" style="color: red;display: none">Senha é igual a anterior.</small>
            </div>
            
            <div class="form-group">
              <label for="pass2">Redigite a senha:</label>
              <input type="password" id="pass2" class="form-control" required>
              <small id="b" style="color: red;display: none">Senhas não coincidem</small>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <button type="submit" id="save" class="btn btn-primary">Salvar Alterações</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  <?php endforeach;?>
  <!--Fim do Modal-->

    <!-- Modal Cadastrar Vários -->
  <div  class="modal fade" id="varios" tabindex="-1" 
        role="dialog" aria-labelledby="cadastro" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="cadastro">Cadastrar vários usuários</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form  action="<?php echo base_url('Usuarios/csv'); ?>" 
              method="post" enctype="multipart/form-data">
          <div class="modal-body">
            <label for="login">
              <ul>
                <li>
                  <a href="<?php echo base_url();?>download/modelo.csv">
                    <b>Download do Arquivo modelo.csv</b>
                  </a>
                </li>
                
                <li>
                  <a href="<?php echo site_url()?>Usuarios/manual">
                    <b>Download da lista de ID/Funcionários</b>
                  </a>
                </li>
              </ul>

              <b>AVISO:</b> Os acessos aos sistemas deverão ser cedidos manualmente.
            </label>
            <br><br>
              <div class="input-group custom-file">
                <input type="file" class="custom-file-input" id="arquivo" name="csv" required/>
                <label class="custom-file-label" for="arquivo">Importe um arquivo .csv</label>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
            <input type="submit" class="btn btn-success form-control" name="csvfile" value="Importar">
          </div>
        </form>
      </div>
    </div>
  
</div>

   <!--Modal Excluir-->
    <?php foreach($usuarios as $row):?>
  <div class="modal fade" id="remover<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir acesso de <?=$row['nome']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Usuarios/dlt');?>
        <input type="hidden" name="id" value="<?=$row['id']?>">
        <h5>Deseja remover permanentemente o acesso de <?=$row['nome']?> ao sistema?</h5>
        <small>Essa ação não pode ser desfeita.</small>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-danger">Excluir</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->

   <!--Modal Editar Acesso-->
    <?php foreach($usuarios as $row):?>
  <div class="modal fade edit_access_modal" name= "acesso<?=$row['id']?>" id="acesso<?=$row['id']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <?php echo form_open('Usuarios/acesso');?>
        <div class="modal-header">
          <h5 class="modal-title">Editar acesso de <?=$row['nome']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <div class="form-group">
            <input type="hidden" name="id_login" class="id_login" value="<?=$row['id'];?>">
            <label for="sistema">Garantir acesso a:</label>
            <?php foreach($sistema as $row2):?>
            <div class="input-group mb-3 first_div">
              <div class="input-group-prepend">
                <div class="input-group-text">
                  <input type="checkbox" name="sistema[]" class="sistema insert_access" 
                          value="<?=$row2['id_sistema']?>" id="sistema_<?=$row2['id_sistema']?>"
                          <?php
                          foreach ($acesso as $row3):
                          //Teste de acesso
                          if(($row['id']==$row3['id_login'])&&
                            ($row2['id_sistema']==$row3['id_sistema'])):?>
                            checked
                          <?php
                          //Selecionar n´´ivel atual
                          switch ($row3['nivel']) {
                            case '1':
                              $nivel1="selected"; 
                              $nivel2=" ";
                              $nivel3=" ";
                            break;

                            case '2':
                              $nivel1=" "; 
                              $nivel2="selected";
                              $nivel3=" ";
                            break;
                            case '3':
                              $nivel1=" "; 
                              $nivel2=" ";
                              $nivel3="selected"; break;
                            default:
                              $nivel1="selected"; 
                              $nivel2=" ";
                              $nivel3=" ";
                            break;
                          }

                          endif;
                          endforeach;
                          ?>
                          >
                </div>
              </div>
              <span class="form-control" for="sistema_<?=$row2['id_sistema']?>">
                <?=$row2['nome']?>
              </span>
              <select class="custom-select nivel" name="nivel[]">
                <option value="1" <?php echo $nivel1;?>>Somente Leitura</option>
                <option value="2" <?php echo $nivel2;?>>Editor</option>
                <option value="3" <?php echo $nivel3;?>>Administrador</option>
              </select>
            </div>
            <?php 
              //Resetar para não selecionado
              $nivel0=$nivel1=$nivel2=$nivel3=" "; 
            endforeach;?>
          </div>
      </div>
    </form>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->
</main>

<script type="text/javascript">
$(document).ready(function(){
    
  $(".sistema").change(function(){
    if ($(this).is(':checked')) {
      $(this).parent().parent().parent().find( '.nivel' ).prop('required', true); 
    }else{
      $(this).parent().parent().parent().find( '.nivel' ).prop('required', false);
    }

  });
  //Validação de senha
  $("#pass1").keyup(function() {
  if ($("#pass0").val() == $("#pass1").val()) {
    $("#a").fadeIn('slow');
    $("#save").prop("disabled",true);
  } else {
    $("#a").fadeOut('slow');
  }
});

    $("#pass2").keyup(function() {
  if ($("#pass1").val() != $("#pass2").val()) {
    $("#b").fadeIn('slow');
    $("#save").prop("disabled",true);
  } else {
    $("#b").fadeOut('slow');
    if(($("#pass0").val() != $("#pass1").val())&&($("#pass0").val() != $("#pass2").val())){
        $("#save").prop("disabled",false);
    }
  }
});

});
</script>