<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php 
//Alerta
if($mensagem!=" "):?>
  <div id="mensagem">
    <div style="padding: 5px;">
      <div id="txt_mensagem" class="alert alert-<?=$alert_type?>">
        <?=$mensagem;?>
        <button class="close" date-dimsiss="alert">&times;</button>
      </div>
    </div>
  </div>
<?php endif;?>
<div>
  <div class="container mt-4">
    <fieldset>
      <legend>Lixeira</legend>
      <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
          <a class="nav-link active" id="funcionario-tab" data-toggle="tab" 
          href="#funcionarios" role="tab" aria-controls="funcionarios" aria-selected="true">
            Funcionários
          </a>
        </li>

        <li class="nav-item">
          <a class="nav-link" id="empresa-tab" data-toggle="tab" 
          href="#empresa" role="tab" aria-controls="empresa" aria-selected="false">
            Empresas
          </a>
        </li>
      </ul>

      <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="funcionarios" 
        role="tabpanel" aria-labelledby="funcionario-tab">
          <table class="table table-sm table-striped">
            <thead>
              <th>Nº Ordem</th>
              <th>Nome</th>
              <th>Ações</th>
            </thead>
            <tbody>
              <?php foreach($funcionarios as $row):?>
              <tr>
                <td><?=$row['nordem']?></td>
                <td><?=$row['nome'];?> <?=$row['sobrenome'];?></td>
                <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" 
                  data-target="#rst_f<?=$row['id_funcionario']?>">
                  <i class="fas fa-trash-restore-alt"></i> Restaurar
                </button>

              <?php if($_SESSION['sys_3']>1): ?>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" 
                  data-target="#dlt_f<?=$row['id_funcionario']?>">
                  <i class="fas fa-user-slash"></i> Deletar
                </button>
              <?php endif;?>
                </td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>

        <div class="tab-pane fade" id="empresa" 
        role="tabpanel" aria-labelledby="empresa-tab">
          <table class="table table-sm table-striped">
            <thead>
              <th>ID</th>
              <th>Nome</th>
              <th>Ações</th>
            </thead>
            <tbody>
              <?php foreach($empresa as $row):?>
              <tr>
                <td><?=$row['id_empresa']?></td>
                <td><?=$row['razao_social'];?></td>
                <td>
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" 
                  data-target="#rst_e<?=$row['id_empresa']?>">
                  <i class="fas fa-trash-restore-alt"></i> Restaurar
                </button>
                <?php if($_SESSION['sys_3']>1): ?>
                <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" 
                  data-target="#dlt_e<?=$row['id_empresa']?>">
                  <i class="fas fa-user-slash"></i> Deletar
                </button>
                <?php endif;?>
                </td>
              </tr>
              <?php endforeach;?>
            </tbody>
          </table>
        </div>
      </div>
    </fieldset>
  </div>

<!--Modal Excluir Funcionários-->
<?php foreach($funcionarios as $row):?>
  <div class="modal fade" id="dlt_f<?=$row['id_funcionario']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir <?=$row['nome']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Lixeira/dlt_f');?>
        <input type="hidden" name="id_funcionario" value="<?=$row['id_funcionario']?>">
        Deseja deletar permanentemente o funcionário <br><h5><?=$row['nome']?> <?=$row['sobrenome']?>?</h5>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-danger">Excluir</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->

  <!--Modal Excluir Funcionários-->
<?php foreach($empresa as $row):?>
  <div class="modal fade" id="dlt_e<?=$row['id_empresa']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Excluir <?=$row['razao_social']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Lixeira/dlt_e');?>
        <input type="hidden" name="id_empresa" value="<?=$row['id_empresa']?>">
        Deseja deletar permanentemente o funcionário <br><h5><?=$row['razao_social']?>?</h5>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-danger">Excluir</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->

  <!--Modal Restaurar Funcionários-->
<?php foreach($funcionarios as $row):?>
  <div class="modal fade" id="rst_f<?=$row['id_funcionario']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Restaurar <?=$row['nome']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Lixeira/rst_f');?>
        <input type="hidden" name="id_funcionario" value="<?=$row['id_funcionario']?>">
        Restaurar funcionário <br><h5><?=$row['nome']?> <?=$row['sobrenome']?>?</h5>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-success">Confirmar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->

  <!--Modal Restaurar Empresas-->
<?php foreach($empresa as $row):?>
  <div class="modal fade" id="rst_e<?=$row['id_empresa']?>" tabindex="-1" role="dialog" aria-labelledby="deletar" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="deletar">Restaurar <?=$row['razao_social']?></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body">
        <?php echo form_open('Lixeira/rst_e');?>
        <input type="hidden" name="id_empresa" value="<?=$row['id_empresa']?>">
        Deseja restaurar a empresa <br><h5><?=$row['razao_social']?>?</h5>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar</button>
          <button type="submit" class="btn btn-success">Confirmar</button>
        </div>
      </form>
      </div>
    </div>
  </div>
</div>
<?php endforeach;?>
  <!--Fim do Modal-->
</div>

</body>
</html>