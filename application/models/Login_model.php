<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Login_model extends CI_Model  {
	
	function Validar_login($login,$senha){
		$table = 'login';
		$dados = array('login' => $login, 'senha' => $senha);
		$query = $this->db->get_where($table, $dados);
		$num_rows = $query->num_rows();
		if($num_rows == 1){
			return $query->row_object();
		}
		else{
			return false;
		}
	}
}
?>