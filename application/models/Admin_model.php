<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin_model extends CI_Model  {
	
	function CheckUser($id){
		$table = 'acesso';
		$dados = array('id_login'=> $id);
		$query = $this->db->get_where($table, $dados);
		return $query->result_array();
	}

	function CheckAcess($id,$system,$level){
		$table = 'acesso';
		$dados = array('id_login' => $id, 'id_sistema' => $system, 'nivel' => $level);
		$query = $this->db->get_where($table, $dados);
		$num_rows = $query->num_rows();
		if($num_rows == 1){
			return $query->row_array();
		}
		else{
			return false;
		}
	}
}
?>