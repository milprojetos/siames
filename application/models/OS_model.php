<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class OS_model extends CI_Model {
	function Select($table,$order){
		$this->db->order_by($order,'ASC');
		$query = $this->db->get($table);
		$results=$query->result_array();
		return $results;
	}

	function Get_Where($table,$condicao){
		$query = $this->db->get_where($table,$condicao);
		return $query->result_array();
	}

	function Responsavel($where){
		$this->db->where('setores.id_setor',$where);
		$this->db->select("	setores.id_setor as 'id', funcionarios.nome, 
							setores.descricao as 'setor', email.email");
		$this->db->from('setores');
		$this->db->join('email', 'email.id_funcionario = setores.id_responsavel');
		$this->db->join('funcionarios', 'funcionarios.id_funcionario=setores.id_responsavel');
		$query = $this->db->get();
		return $query->result_array();
	}
	
	function Select_where($table,$condition){
		$query = $this->db->get_where($table,$condition);
		return $query->result_array();
	}

	function Update($where,$id,$table,$dados){
		$this->db->where($where, $id);
		$this->db->update($table, $dados);		
	}
	function Delete($where,$id,$table){
		$this->db->where($where, $id);
		$this->db->delete($table);	
	}
}
?>