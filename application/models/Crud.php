<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Crud extends CI_Model {
	function Insert($table, $dados){
		$this->db->insert($table, $dados);
		$last_id = $this->db->insert_id();
		
		if($last_id==0){
			$last_id = null;
		}

		return $last_id;			
	}
	
	function Insert_checked($table, $dados){
		$this->db->insert($table, $dados);
		$array['last_id'] = $this->db->insert_id();
		$array['check'] = ($this->db->affected_rows() == 1) ? true : false;

		return $array;			
	}
	
	function Select($table,$order){
		$this->db->order_by($order,'ASC');
		$query = $this->db->get($table);
		$results=$query->result_array();
		return $results;
	}

	function Select_where_order($table,$condicao,$order_by){
		$this->db->order_by($order_by,'ASC');
		$query = $this->db->get_where($table,$condicao);
		return $query->result_array();
	}
	
	function count_if($table,$dados){
		$query = $this->db->get_where($table,$dados);
		return $query->num_rows();
	}

	function Select_wherein($table,$condition,$in){
		$this->db->order_by($condition);
		$this->db->or_where_in($condition,$in);
		$query = $this->db->get($table);
		return $query->result_array();
	}
	
	function Select_where($table,$condition){
		$query = $this->db->get_where($table,$condition);
		return $query->result_array();
	}

	function Update($where,$id,$table,$dados){
		$this->db->where($where, $id);
		$this->db->update($table, $dados);		
	}
	
	function Update_where($table,$dados,$where){
		$this->db->update($table, $dados,$where);
		$confirm = $this->crud->count_if($table, $dados);
		return $confirm;
	}
	
	function Delete_where_data($table,$dados){
		$this->db->delete($table, $dados);
		
		return TRUE;
		$this->load->model('crud');
		$return = $this->crud->count_if($table,$dados);
		if($return==0){
			echo 'true';
		}else{
			echo 'false';
		}
		
	}
	
	function Delete($column_name,$cell_data,$table){
		$this->db->where($column_name, $cell_data);
		$this->db->delete($table);
		
		$this->db->where($column_name, $cell_data);
		$query = $this->db->get($table);	
		$result = $query->num_rows();
		if($result==0){
			echo 'true';
		}else{
			echo 'false';
		}
	}
}
?>