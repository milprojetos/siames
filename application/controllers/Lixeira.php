<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lixeira extends CI_Controller{

	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'3');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="restaurado(a)";break;
					case 2:$action="deletado(a)";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Funcionário '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Funcionário não '.
						$action.
						', ocorreu um <strong>erro</strong>.';
					break;

					case 3:
						$dados['alert_type']='success';
						$dados['mensagem']='Empresa '.$action.' com <strong>sucesso</strong>.';
					break;

					case 4:
						$dados['alert_type']='danger';
						$dados['mensagem']='Empresa não '.
						$action.
						', ocorreu um <strong>erro</strong>.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}

			$this->load->model('crud');
			$um=array('1' => '1');
			$ativo=array('ativo' => '0');
			$dados['funcionarios']=
				$this->crud->Select_where_order('funcionarios',$ativo,'id_funcionario');
			$dados['empresa']=
				$this->crud->Select_where_order('empresa',$ativo,'id_empresa');
			$this->load->view('admin/header_adm_view',$dados);
			$this->load->view('admin/lixeira_view',$dados);
		}else{
			redirect('selecao');
		}
	}


public function dlt_f(){
	$where='id_funcionario';
	$table='funcionarios';
	$id=$_POST['id_funcionario'];	
	$this->load->model('crud');
	$this->crud->Delete($where,$id,$table);
	$this->db->affected_rows() > 0 ? 
	redirect('lixeira?msg=1&type=2') : 
	redirect('lixeira?msg=2&type=2');
}

public function dlt_e(){
	$where='id_empresa';
	$table='empresa';
	$id=$_POST['id_empresa'];	
	$this->load->model('crud');
	$this->crud->Delete($where,$id,$table);
	$this->db->affected_rows() > 0 ? 
	redirect('lixeira?msg=3&type=2') : 
	redirect('lixeira?msg=4&type=2');
}

public function rst_f(){
	$where='id_funcionario';
	$table='funcionarios';
	$id=$_POST['id_funcionario'];
	$data = array('ativo' => '1');
	
	$this->load->model('crud');
	$this->crud->Update($where,$id,$table,$data);
	$this->db->affected_rows() > 0 ? 
	redirect('lixeira?msg=1&type=1') : 
	redirect('lixeira?msg=2&type=1');

}

public function rst_e(){
	$where='id_empresa';
	$table='empresa';
	$id=$_POST['id_empresa'];
	$data = array('ativo' => '1');
	
	$this->load->model('crud');
	$this->crud->Update($where,$id,$table,$data);
	$this->db->affected_rows() > 0 ? 
	redirect('lixeira?msg=3&type=1') : 
	redirect('lixeira?msg=4&type=1');

}

}
?>