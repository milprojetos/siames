<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionario extends CI_Controller{

	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'1');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="cadastrado(s)";break;
					case 2:$action="editado(s)";break;
					case 3:$action="excluido(s)";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Funcionário(s) '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Funcionário(s) não '.$action.', ocorreu um <strong>erro</strong>.';
					break;

					case 'fbd403':
						$dados['alert_type']='danger';
						$dados['mensagem']='<strong>Erro!</strong> Você não possui permissão para utilizar essa função.';
					break;

					case 3:
						$dados['alert_type']='danger';
						$dados['mensagem']='Funcionário(s) não '.$action.', <strong>já existe um funcionário que utiliza um dos e-mails cadastrados como seu e-mail principal</strong>.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}

			
			$this->load->model('crud');
			$ativo=array('ativo' => '1');
			$dados['select']=$this->crud->Select_where_order('funcionarios',$ativo,'nome');
			$dados['empresa']=$this->crud->Select('empresa','1');
			$dados['funcao']=$this->crud->Select('funcao','1');
			$dados['setor']=$this->crud->Select('setores','1');
			$dados['email']=$this->crud->Select('email','1');
			$this->load->view('sys_ponto/dashboard_view',$dados);
			$this->load->view('sys_ponto/funcionario_view',$dados);
			$this->load->view('sys_ponto/footer');
		}else{
			redirect('Selecao');
		}
	}

	public function verify_duplicity($table,$dados){
		$this->load->model('crud');
		$return = $this->crud->count_if($table,$dados);
		
		if($return>0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}
		
	public function add(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){

		$ilegal = 0;
		$email_type=0;
		//Captura o e-mail e o converte para minúsculo
		$emails=$_POST['email'];
		$emails = array_map('strtolower', $emails);
		//Captura o $_POST e o converte para maiúsculo
		$_POST = array_map('strtoupper', $_POST);
		$data = array(	'id_empresa' => $_POST['empresa'],
						'nordem' => $_POST['nordem'],
						'nome' => $_POST['empregado'],
						'sobrenome' => $_POST['sobrenome'], 
						'num_ctps' => $_POST['nctps'],  
						'num_reg' => $_POST['nreg'], 
						'id_funcao' => $_POST['funcao'],
						'id_setor' => $_POST['setor'], 
						'horario' => $_POST['horario'],
						'ativo' => '1');
		
		//----------Validação e tratamento de dados----------				
		if($_POST['nctps']=='' || empty($_POST['nctps'])){
			$data['num_ctps'] = '??????';
		}
		
		if($_POST['nreg']=='' || empty($_POST['nreg'])){
			$data['num_reg'] = '??????';
		}
		
		$data_verify = array('email'=>$emails[0]);
		$duplicity = $this->verify_duplicity('email',$data_verify);
		if($duplicity){
			redirect('funcionario?msg=3&type=1');
			$ilegal = 1;
		}
		
		//o bloco a seguir validará os emails inserindo apenas aqueles não duplicados
		for($i = 0;count($emails)>$i;$i++){
			for($j = 0;count($emails)>$j;$j++){
				if($emails[$i]==$emails[$j]&&$i!=$j){
					$emails[$j] = '';
				}
			}
		}
		//---------------------------------------------------
		if($ilegal==0){
			//Deixa as primeiras letras de cada palavra Maiúscula e o resto Minúscula
			$apelido=ucwords(strtolower($data['nome']));
			$this->load->model('crud');
			//Cadastra o funcionário
			$last = $this->crud->insert('funcionarios',$data);

			//Para cada e-mail enviado por $_POST
			//email primario tipo 0 secundario tipo 1
			foreach ($emails as $email) {
				if($email!='' || !empty($email)){
					if($email_type==0){
						$dados = array('email'=>$email, 'id_funcionario'=>$last);
					}elseif($email_type>0){
						$dados = array('email'=>$email, 'id_funcionario'=>$last,'tipo'=>$email_type);
					}
					//Salva o e-mail
					$this->crud->insert('email',$dados);    
					//Se for o primeiro e-mail 
					if($email_type==0){
						//Quebra o e-mail antes do @
						list($login,$lixo) = explode("@", $email);

						//Cadastra o login e a senha como o identificador do e-mail
						$dados_login = array('login'=>$login,
											'senha'=>$login,
											'nome'=>$apelido,
											'id_funcionario'=>$last
											);
						$this->crud->insert('login',$dados_login);
						$email_type=1;
					}
				}
				else{
					redirect('funcionario?msg=2&type=1');
				}
			}
		}
		redirect('funcionario?msg=1&type=1');
	}
		else{
			redirect('funcionario?msg=fbd403&type=');
		}
	}

	public function edt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){

		$_POST = array_map('strtoupper', $_POST);
		$emails=$_POST['email'];
		$emails = array_map('strtolower', $emails);
		$where='id_funcionario';
		$table='funcionarios';
		$id=$_POST['id_funcionario'];
		$dados = array(	'id_empresa' => $_POST['empresa'],
						'nome' => $_POST['empregado'],
						'sobrenome' => $_POST['sobrenome'], 
						'num_ctps' => $_POST['nctps'], 
						'nordem' => $_POST['nordem'], 
						'num_reg' => $_POST['nreg'], 
						'id_funcao' => $_POST['funcao'],
						'id_setor' => $_POST['setor'], 
						'horario' => $_POST['horario']);
			
		$this->load->model('crud');
		$this->crud->Update($where,$id,$table,$dados);
		
		foreach ($emails as $email) {
			if($email!='' || !empty($email)){
				$dados = array('email'=>$email, 'id_funcionario'=>$id);
				
				$duplicity = $this->verify_duplicity('email',$dados);
				if($duplicity){
					echo 'Este usuario já possui um email identico';
				}elseif(!$duplicity){
					$dados['tipo'] = '1';
					//Salva o e-mail
					$this->crud->insert('email',$dados);
				}
			}
			else{
				redirect('funcionario?msg=2&type=2');
			}

		}
		$this->db->affected_rows() > 0 ? 
		redirect('funcionario?msg=1&type=2') : 
		redirect('funcionario?msg=2&type=2');
	}
		else{
			redirect('funcionario?msg=fbd403&type=');
		}
	}
		
	public function dlt(){
		$validate_return;
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level==3){
			$where='id_funcionario';
			$table='funcionarios';
			$id=$_POST['id_funcionario'];
			$data = array('ativo' => '0');
			
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$data);
			$this->db->affected_rows() > 0 ? 
			$validated_return = 'funcionario?msg=1&type=3' : 
			$validated_return = 'funcionario?msg=2&type=3';
			
			$delete_where = array('id_funcionario' => $id);
			//desvincular email	
			$this->load->model('crud');
			$return = $this->crud->Delete_where_data('email',$delete_where);
			if($return=='true'){
				echo 'emails excluidos com sucesso';
			}else{
				echo 'Ouve um ero na exclusão dos email do funcionario deletado';
			}
			
			//desvincular login
			$this->load->model('crud');
			$return = $this->crud->Delete_where_data('login',$delete_where);
			if($return=='true'){
				echo 'login excluido com sucesso';
			}else{
				echo 'Ouve um ero na exclusão dos login do funcionario deletado';
			}
			redirect($validated_return);
		}
		else{
			redirect('funcionario?msg=fbd403&type=');
		}
	}
	
	public function dlt_email(){
		$id_email = $_POST['id'];
		
		$this->load->model('crud');
		$return = $this->crud->Delete('id_email',$id_email,'email');
		return $return;
	}


	//IMPORTAÇÃO E EXPORTAÇÃO DE ARQUIVOS
	public function csv(){
	$id_login = $_SESSION['id'];
	$access_level = isset($_SESSION['sys_1']);
	if(isset($access_level)&&$access_level==3){
		$this->load->model('crud');
		if($this->input->post('csvfile')){
			if(is_uploaded_file($_FILES['csv']['tmp_name'])){
				$this->load->library('CSVReader');
				$arquivo = $this->csvreader->parse_csv($_FILES['csv']['tmp_name']);
			
				if(!empty($arquivo)){
					foreach($arquivo as $row){
						//Captura o e-mail e o converte para minúsculo
						$email=strtolower($row['Email_para_Contato']);
						//Captura os dados e converte para maiúsculo
						$row = array_map('strtoupper', $row);
						$data = array('nordem' => $row['Numero_de_ordem'],
									'nome' => $row['Nome'],
									'sobrenome' => $row['Sobrenome'],
									'num_ctps' => $row['Carteira_de_Trabalho'],
									'num_reg' => $row['RG'],
									'id_funcao'=>$row['Codigo_da_Funcao'],
									'id_empresa' => $row['Codigo_da_Empresa'],
									'id_setor'=> $row['Codigo_do_Setor'],
									'horario' => $row['horario'],
									'ativo'=>'1'
									);
						$apelido=ucwords(strtolower($data['nome']));
						//Cadastra funcionário
						$last = $this->crud->insert('funcionarios',$data);
												
						$dados = array('email'=>$email, 'id_funcionario'=>$last);
						//Salva o e-mail
						$this->crud->insert('email',$dados);
						//Quebra o e-mail antes do @
						list($login,$lixo) = explode("@", $email);
						//Cadastra o login e a senha como o identificador do e-mail
						$dados_login = array('login'=>$login,
											'senha'=>$login,
											'nome'=>$apelido,
											'id_funcionario'=>$last
											);
						$this->crud->insert('login',$dados_login);
					}
				}
				else{
					redirect('funcionario?msg=2&type=1');
				}
			}
			//Quando finalizar todas as linhas
			$this->db->affected_rows() > 0 ? 
			redirect('funcionario?msg=1&type=1') : 
			redirect('funcionario?msg=2&type=1');
		}
	}
		else{
			redirect('funcionario?msg=fbd403&type=');
		}
	}

		public function manual(){
		// nome
        $nome = 'Manual de Cadastro CSV.xlsx';  
		// Carregar Library
        $this->load->library('excel');
        $this->load->model('crud');
        $empresas=$this->crud->Select('empresa','id_empresa');
        $funcao=$this->crud->Select('funcao','id_funcao');
        $setor=$this->crud->Select('setores','id_setor');

        $excel = new PHPExcel();
        //Página 0
        $excel->setActiveSheetIndex(0);
        // Cabeçalho Empresas
        $excel->getActiveSheet()->setTitle('Empresas');
        $excel->getActiveSheet()->SetCellValue('A1', 'Código da Empresa');
        $excel->getActiveSheet()->SetCellValue('B1', 'Nome da Empresa');
        // Linha
        $linha = 2;
        foreach ($empresas as $row) {
            $excel->getActiveSheet()->SetCellValue('A' . $linha, $row['id_empresa']);
            $excel->getActiveSheet()->SetCellValue('B' . $linha, $row['razao_social']);
            $linha++;
        }

        $excel->createSheet(1);
        //Página 1
        $excel->setActiveSheetIndex(1);
        // Cabeçalho Função
        $excel->getActiveSheet()->setTitle('Funções');
        $excel->getActiveSheet()->SetCellValue('A1', 'Código da Função');
        $excel->getActiveSheet()->SetCellValue('B1', 'Descrição da função');
        // Linha
        $linha = 2;
        foreach ($funcao as $row) {
            $excel->getActiveSheet()->SetCellValue('A' . $linha, $row['id_funcao']);
            $excel->getActiveSheet()->SetCellValue('B' . $linha, $row['descricao']);
            $linha++;
        }

        $excel->createSheet(2);
        //Página 2
        $excel->setActiveSheetIndex(2);
        // Cabeçalho Setor
        $excel->getActiveSheet()->setTitle('Setores');
        $excel->getActiveSheet()->SetCellValue('A1', 'Código do Setor');
        $excel->getActiveSheet()->SetCellValue('B1', 'Nome do Setor');
        // Linha
        $linha = 2;
        foreach ($setor as $row) {
            $excel->getActiveSheet()->SetCellValue('A' . $linha, $row['id_setor']);
            $excel->getActiveSheet()->SetCellValue('B' . $linha, $row['descricao']);
            $linha++;
        }

        $excel->setActiveSheetIndex(0);
		// Download 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$nome.'"');
		header('Cache-Control: max-age=0');
		$documento = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$documento->save('php://output');
    }
}
?>