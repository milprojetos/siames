<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sys_Aluno extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		if(isset($access_level)&&$access_level>=1){
			redirect('aluno');
		}else{
			redirect('selecao');
		}
	}
}
?>