<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OS extends CI_Controller {
	public function index()
	{
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_2']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'2');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			$dados['select_setor']=$this->crud->Select('setores','id_setor');	
			$dados['select_assunto']=$this->crud->Select('assunto_ref','id');
			$dados_usu=array('id' => $this->session->id);
			$dados['select_usu']=$this->crud->Select_where('usuarios',$dados_usu);
			$dados_func=array('id_funcionario' => $dados['select_usu'][0]['funcionario']);
			$dados['select_ficha']=$this->crud->Select_where('ficha',$dados_func);
			$this->load->view('os/header_os',$dados);
			$this->load->view('os/os_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function nova_os(){
		$dados=array(
		'id_solicitador' => $_POST['id'],
		'id_setor_dest' => $_POST['setor_destino'],
		'prioridade' => $_POST['prioridade'],
		'id_assunto' => $_POST['assunto'],
		'descricao' => $_POST['descricao'],
		'data' => $_POST['data']);
		
		$this->load->model('crud');
		$validacao = $this->crud->Insert('ordem_servico', $dados);
		if(isset ($validacao)){
			$dados_relacao = array('id_requisicao'=>$validacao, 'status'=> 0);
			$this->load->model('crud');
			$this->crud->Insert('relacao_solicitacao', $dados_relacao);
			
			$this->os_email($_POST);
			redirect('OS');
		}else{
			echo 'Não foi possivel inserir um novo sistema';
			redirect('OS');
		}
	}
	
	public function os_email($dados){

		$this->load->config('email');
		$this->load->library('email');
		$this->load->model('OS_model');
		$from = $this->config->item('smtp_user');
		//Nome do assunto, cadastrado no Banco
		$array= array('id_assunto'=>$dados['assunto']);
		$assuntos=$this->OS_model->Get_Where('assunto',$array);
		//Responsável do Setor
		$setor_destino=$this->OS_model->Responsavel(1);
		$apelido=ucwords(strtolower($setor_destino[0]['nome']));
		//$to = $this->input->post('to');
		//$subject = $this->input->post('subject');
		//Formato da DATA
		$bad_date = $dados['data'];
		$date = nice_date($bad_date, 'd/m/Y');

		
		$message = "<html><body><p style='color:black;'>Solicitação:".$dados['id']." - Data: ". $date."</p>
		 <p style='color:black;'>Dados do solicitador</p>
		 <p style='color:black;'>Nome: ".$dados['emissor']."</p>
		 <p style='color:black;'>Setor: ".$dados['setor_origem']." - Função:".$dados['funcao']."</p>
		 <p style='color:black;'>Para: ".$dados['setor_destino']." - Prioridade: ".$dados['prioridade']."</p><br>
		 <p style='color:black;'>Mensagem:</p>
		 <p style='color:black;'>".$dados['descricao']."</p></body></html>";
		//Dados para a View
		$info = array(
				"id"=>$dados['id'],
				"date"=>$date,
				"assunto"=>$assuntos[0]['assunto'],
				"emissor"=>$dados['emissor'],
				"setor_origem"=>$dados['setor_origem'],
				"funcao"=>$dados['funcao'],
				"setor_destino"=>$setor_destino[0]['setor'],
				"responsavel"=>$apelido,
				"prioridade"=>$dados['prioridade'],
				"descricao"=>$dados['descricao']
			);
		//Mensagem HTML
		$message=$this->load->view('email/email_template', $info,true);
		$this->email->set_newline("\r\n");
		$this->email->from($from);
		$this->email->to($setor_destino[0]['email']);
		$this->email->subject($info['assunto']);
		$this->email->message($message);


		if ($this->email->send()) {
            $this->index();
        } else {
            show_error($this->email->print_debugger());
        }
	}
}
?>