<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portaria extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_4']);
		if(isset($access_level)&&$access_level>=1){
			redirect('info');
		}else{
			redirect('selecao');
		}
	}

	public function Info(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_4']);
		if(isset($access_level)&&$access_level>=1){
			redirect('info');
		}else{
			redirect('selecao');
		}
	}
}
?>