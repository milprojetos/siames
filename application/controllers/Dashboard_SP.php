<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_SP extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){
			redirect('dashboard_SP/funcionario');
		}else{
			redirect('selecao');
		}
	}
	public function funcionario(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){
			redirect(base_url('funcionario'));
		}else{
			redirect('selecao');
		}
	}

	public function empresa(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){
			redirect('empresa');
		}else{
			redirect('selecao');
		}
	}

	public function imprimir(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){
			$this->load->view('sys_ponto/dashboard_view');
			$this->load->view('sys_ponto/imprimir_view');
			$this->load->view('sys_ponto/footer');
		}else{
			redirect('selecao');
		}
	}

	public function fichas(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		if(isset($access_level)&&$access_level>=1){
			$this->load->view('sys_ponto/A4');
		}else{
			redirect('selecao');
		}
	}
}
?>