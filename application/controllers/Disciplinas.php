<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Disciplinas extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_7']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'7');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			/*-----Aqui deve ser inserida as arrays de dados que alimentara a view-----*/
			$dados['disciplinas']=$this->crud->Select('disciplinas','nome');
			$dados['grades']=$this->crud->Select('grade_curricular','nome');
			$dados['semestres']=$this->crud->Select('semestre','nome_sem');	
			/*-------------------------------------------------------------------------*/
			$this->load->view('sys_acerto/header_acerto_view',$dados);
			$this->load->view('sys_acerto/disciplina_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function add(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_7']);
		if(isset($access_level)&&$access_level>=1){
			$data_disc = array(
				'nome' => $_POST['nome'],
				'carga_horaria' => $_POST['carga_horaria'],
				'id_grade_cur' => $_POST['grade'],
				'sem_indicado' => $_POST['sem_indicado'],
				'cod_tipo_disc' => $_POST['tipo']
			);
			$this->load->model('crud');
			$insert_check = $this->crud->Insert_checked('disciplinas',$data_disc);
			
			if($insert_check['check']){
				redirect('disciplinas');
			}elseif(!$insert_check['check']){
				redirect('disciplinas');
			}
		}
	}
}
?>