<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller{
	public function index(){
		parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
		$data['mensagem']=' ';
		if(isset($_GET['msg'])){
			$a=$_GET['msg'];
			switch($a)
			{
				case 1: $data['mensagem'] = 'Usuário ou senha inválidos. Verifique os dados e tente novamente.';break;
				case 2: $data['mensagem'] = 'Tipo de usuário inválido. Por favor, contrate um administador.';break;
				default:$data['mensagem'] = ' ';break;
			}
		}
		$this->load->view('login_view',$data);
	}
	
	public function logout(){
		$this->session->sess_destroy();
		$data['mensagem']=' ';
		$this->load->view('login_view',$data);
	}

	public function senha(){
		//Dados vindos do Post
		$senha_antiga=$_POST['senha'];
		$senha_nova=$_POST['senha2'];
		$this->load->model('Crud');
		$dados=array('senha'=>$senha_nova);
		//Nova senha
		$this->Crud->Update('login',$senha_antiga,'login',$dados);
		$sessao = array('senha'=>$senha_nova);
		$this->session->set_userdata($sessao);
		redirect('selecao');
	}

	public function validacao(){
		$login=$_POST['login'];
		$senha=$_POST['senha'];
		$this->load->model('Login_model');
		$this->load->model('Crud');
		$data = $this->Login_model->Validar_login($login,$senha);
			if($data==FALSE){
				//Login Inválido
				redirect('login?msg=1');
			}
			else{
				$id = $data->id;
				$senha= $data->senha;
                $nome = $data->nome;
				$sessao = array('id'=>$id,'senha'=>$senha,'nome'=>$nome);
				$this->session->set_userdata($sessao);
					if($login==$senha){
					//Primeiro Login
						$this->load->view('primeiro_acesso_view',$sessao);
					}
					else{
						redirect('selecao');
					}
				}
		}
}
?>