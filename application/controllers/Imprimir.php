<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Imprimir extends CI_Controller{

	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'1');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			$header=array('ativo'=>'1');
			$dados['select']=$this->crud->Select_where_order('ficha',$header,'nome');
			$this->load->view('sys_ponto/dashboard_view',$dados);
			$this->load->view('sys_ponto/imprimir_view',$dados);
			$this->load->view('sys_ponto/footer');
		}else{
			redirect('selecao');
		}
	}

public function get_dados(){
//Vazio
$dados['vazio']=$_POST['vazio'];
unset($_POST['vazio']);
//Pega mês e ano do input date
$separado = explode("-",$_POST['mesano']);
$ano=$separado[0];
switch ($separado[1]) {
    case "01": $mes="JANEIRO"; break;
    case "02": $mes="FEVEREIRO"; break;
    case "03": $mes= "MARÇO"; break;
    case "04": $mes= "ABRIL"; break;
    case "05": $mes= "MAIO"; break;
    case "06": $mes= "JUNHO"; break;
    case "07": $mes= "JULHO"; break;
    case "08": $mes= "AGOSTO"; break;
    case "09": $mes= "SETEMBRO"; break;
    case "10": $mes= "OUTUBRO"; break;
    case "11": $mes= "NOVEMBRO"; break;
    case "12": $mes= "DEZEMBRO"; break;
}
$i=1;
$imprimir=array();
foreach ($_POST as $a=>$b) {if($i!=1){
	$imprimir[$i-2]=$b;}$i++;

}
$this->load->model('crud');
$dados['ano']=$ano;
$dados['mes']=$mes;
$dados['select']=$this->crud->Select_wherein('ficha','nordem',$imprimir);
$this->load->view('sys_ponto/A4',$dados);
}


}
?>