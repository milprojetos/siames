<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sys_Acerto extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_7']);
		if(isset($access_level)&&$access_level>=1){
			redirect('grades');
		}else{
			redirect('selecao');
		}
	}
}
?>