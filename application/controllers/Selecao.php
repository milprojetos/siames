<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Selecao extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		if(isset($id_login))
		{
			$filter_array = array('id_login' => $id_login, 'ativo' => '1');
			$this->load->model('crud');
			$dados['sistemas']=$this->crud
							->Select_where_order('filter_access',$filter_array,'nome');
							
			foreach($dados['sistemas'] as $row){
				$id_sistema = $row['id_sistema'];
				$nivel = $row['nivel'];
				
				$sessao["sys_".$id_sistema] = $nivel;
			}
			
			if(isset($sessao))
			{
				$this->session->set_userdata($sessao);	
			}
			
			$this->load->view('selecao_view', $dados);
		}else{
			echo 'não pode';
			redirect('login');
		}
	}
}
?>