<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'3');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="cadastrado(s)";break;
					case 2:$action="editado(s)";break;
					case 3:$action="excluido(s)";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Usuário(s) '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Usuário(s) não '.
						$action.
						', ocorreu um <strong>erro</strong>.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}

			$this->load->model('crud');
			$null=array('id_funcionario' => NULL);
			$dados['acesso']=$this->crud->Select('acesso','id_acesso');
			$dados['sistema']=$this->crud->Select('sistemas','1');
			$dados['funcionarios']=$this->crud->Select_where_order('sem_login',$null,'nome');
			$dados['usuarios']=$this->crud->Select('usuarios','id');
			$this->load->view('admin/header_adm_view',$dados);
			$this->load->view('admin/usuarios_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function verify_duplicity($table,$dados){
		$this->load->model('crud');
		$return = $this->crud->count_if($table,$dados);
		
		if($return>0){
			return TRUE;
		}
		else{
			return FALSE;
		}
	}

	public function add_usuario(){
		//Filtra os dados vazios vindos de Nível
		$nivel_limpo=array_filter($_POST['nivel']);
		$nivel_limpo=array_values($nivel_limpo);
		//Dados da tabela Login
		$login = array(	'id' => '',
						'nome' => $_POST['nome'],
						'login' => $_POST['login'],
						'senha' => $_POST['senha'], 
						'id_funcionario' => $_POST['funcionario']);
						
		//Validação de Dados
		$data_verify = array('login'=>$login['login']);
		$duplicity = $this->verify_duplicity('login',$data_verify);
		if($duplicity){
			//redirect('');
			$ilegal = 1;
		}
		
		if($ilegal==0){
			//Chamar CRUD
			$this->load->model('crud');
			//Inserir Login
			$last = $this->crud->insert('login',$login);
			
			//Contador
			$i=0;
			//Para cada checkbox
			foreach ($this->input->post('sistema') as $sistema) {
			//Nível que corresponde ao sistema
			$nivel=$nivel_limpo[$i];
			$acesso = array('id_login' =>  $last,
							'id_sistema' => $sistema,
							'nivel' => $nivel);
			$i++;
			$this->crud->insert('acesso',$acesso);
			}

			$this->db->affected_rows() > 0 ? 
			redirect('usuarios?msg=1&type=1') : 
			redirect('usuarios?msg=2&type=1');
		}else{
			redirect('usuarios');
		}
	}

	public function senha(){
		$where='id';
		$table='login';
		$id=$_POST['id'];
		$dados = array(	'senha' => $_POST['senha'],);
		
		$this->load->model('crud');
		$this->crud->Update($where,$id,$table,$dados);
		redirect('usuarios');
	}

	public function insert_access(){
		$id_login=$_POST['id_log'];
		$id_sistema=$_POST['id_sys'];
		$access_level=$_POST['level_val'];
		
		$dados = array('id_login' =>  $id_login,
						'id_sistema' => $id_sistema,
						'nivel' => $access_level);
		
		$this->load->model('crud');
		$return = $this->crud->Insert('acesso',$dados);
		if(isset($return)){
			$nivel = $access_level;
			$sessao['sys_'.$id_sistema] = $nivel;
		
			$this->session->set_userdata($sessao);
			echo 'insert';
		}else{
			echo 'Ouve uma falha ao atribuir acesso a este sistema';
		}
	}
	
	public function remove_access(){
		
		$id_login=$_POST['id_log'];
		$id_sistema=$_POST['id_sys'];
		
		$dados = array('id_login' =>  $id_login,
						'id_sistema' => $id_sistema
						);
						
		$this->load->model('crud');
		$return = $this->crud->Delete_where_data('acesso',$dados);
		if(isset($return)){
			unset($_SESSION['sys_'.$id_sistema]);
			echo 'remove';
		}else{
			echo 'Ouve uma falha ao remover acesso a este sistema';
		}
	}
	
	public function edit_access(){
		$id_login=$_POST['id_log'];
		$id_sistema=$_POST['id_sys'];
		$access_level = $_POST['level_val'];
		$where = array('id_login' =>  $id_login,
						'id_sistema' => $id_sistema);
		$dados = array('id_login' =>  $id_login,
						'id_sistema' => $id_sistema,
						'nivel' => $access_level);
		
		$this->load->model('crud');
		$confirm = $this->crud->Update_where('acesso',$dados,$where);
		
		if($confirm==1){
			echo 'Modificado com sucesso';
		}elseif($confirm==0){
			echo 'Ouve uma falha ao editar acesso a este sistema';
		}else{
			echo 'Um erro inesperado ocorreu';
		}
	}
	
	public function dlt(){
		$where='id';
		$id=$_POST['id'];
		$table='login';

		$this->load->model('crud');
		$this->crud->Delete($where,$id,$table);
		$this->db->affected_rows() > 0 ? 
		redirect('usuarios?msg=1&type=3') : 
		redirect('usuarios?msg=2&type=3');
	}

	public function csv(){
		$this->load->model('crud');
		if($this->input->post('csvfile')){
			if(is_uploaded_file($_FILES['csv']['tmp_name'])){
			 	$this->load->library('CSVReader');
			 	$arquivo = $this->csvreader->parse_csv($_FILES['csv']['tmp_name']);
			 		if(!empty($arquivo)){
			 		 	foreach($arquivo as $row){
			 		 		$data = array(
                                'login' => $row['Login'],
                                'senha' => $row['Senha'],
                                'nome' => $row['Apelido_no_Sistema'],
                                'id_funcionario' => $row['ID_do_Funcionario']
                            );
			 		 		$this->crud->insert('login',$data);

			 		 	}

			 		 }
			 }
			 else{
			 	redirect('usuarios?msg=2&type=1');
			 }
			 //Quando finalizar todas as linhas
			 redirect('usuarios?msg=1&type=1');
		}
	}

	public function manual(){
		// nome
        $nome = 'Código dos Funcionários -'.date("Y - m - d").'.xlsx';  
		// Carregar Library
        $this->load->library('excel');
        $this->load->model('crud');
        $funcionarios=$this->crud->Select('funcionarios','id_funcionario');

        $excel = new PHPExcel();
        //Página 0
        $excel->setActiveSheetIndex(0);
        // Cabeçalho Empresas
        $excel->getActiveSheet()->setTitle('Funcionarios');
        $excel->getActiveSheet()->SetCellValue('A1', 'ID');
        $excel->getActiveSheet()->SetCellValue('B1', 'Nome do Funcionário');
        // Linha
        $linha = 2;
        foreach ($funcionarios as $row) {
            $excel->getActiveSheet()->SetCellValue('A' . $linha, $row['id_funcionario']);
            $excel->getActiveSheet()->SetCellValue('B' . $linha, $row['nome'].' '.$row['sobrenome']);
            $linha++;
        }
		// Download 
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$nome.'"');
		header('Cache-Control: max-age=0');
		$documento = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$documento->save('php://output');
    }
}
?>