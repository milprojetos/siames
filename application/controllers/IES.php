<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ies extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'6');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			/*-----Aqui deve ser inserida as arrays de dados que alimentar�o a view-----*/
			$dados['ies']=$this->crud->Select_where_order('ies','ativo = 1','id_ies');
			/*-------------------------------------------------------------------------*/
			$this->load->view('alunos/header_alunos_view',$dados);
			$this->load->view('alunos/ies_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function add(){	
		$dados=array('nome_ies' => strtoupper($_POST['nome']),
												'emec_ies' => $_POST['emec'],
												'ativo' => '1');
		$this->load->model('crud');
		$this->crud->insert('ies',$dados);
		//$this->db->affected_rows() > 0 ? 
		//redirect('funcoes?msg=1&type=1') : 
		//redirect('funcoes?msg=2&type=1');
		redirect('ies');
	}
	
	public function edt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		if(isset($access_level)&&$access_level>=1){
			$where='id_ies';
			$table='ies';
			$id=$_POST['id_ies'];
			
			$dados = array('nome_ies' => $_POST['nome_ies'],
							'emec_ies' => $_POST['emec_ies']);
			
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$dados);
			$this->db->affected_rows() == 1 ? 
				redirect('ies') : //controle de mensagem de erro(entrou dados)
				redirect('ies');//controle de mensagem de erro (n�o entrou dados)
		}
		//futuro else para erro de permis�o
	}
	
	public function dlt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		
		if(isset($access_level)&&$access_level==3){
			$where='id_ies';
			$table='ies';
			$id=$_POST['id_ies'];
			$data = array('ativo' => '0');
			
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$data);
			$this->db->affected_rows() == 1 ? 
				redirect('ies') : //controle de mensagem de erro(entrou dados)
				redirect('ies');//controle de mensagem de erro (n�o entrou dados)
			//redirect('empresa?msg=1&type=3') : 
			//redirect('empresa?msg=2&type=3');
		}
			else{
				redirect('ies');
				//redirect('empresa?msg=fbd403&type=');
			}
	}
}
?>