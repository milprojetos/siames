<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcoes extends CI_Controller{

	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'3');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="cadastrada";break;
					case 2:$action="editada";break;
					case 3:$action="excluida";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Função '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Função não '.$action.', ocorreu um <strong>erro</strong>.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}
			$this->load->model('crud');
			$ativo=array('1' => '1');
			$dados['select']=$this->crud->Select_where_order('funcao',$ativo,'id_funcao');
			$this->load->view('admin/header_adm_view',$dados);
			$this->load->view('admin/funcao_view',$dados);
		}else{
			redirect('selecao');
		}
	}

public function add(){	
	$dados=array('descricao'=>strtoupper($_POST['funcao']));
	$this->load->model('crud');
	$this->crud->insert('funcao',$dados);
	$this->db->affected_rows() > 0 ? 
	redirect('funcoes?msg=1&type=1') : 
	redirect('funcoes?msg=2&type=1');
}

public function edt(){
	$where='id_funcao';
	$table='funcao';
	$id=$_POST['id_funcao'];
	$dados=array('descricao'=>strtoupper($_POST['funcao']));
	$this->load->model('crud');
	$this->crud->Update($where,$id,$table,$dados);
	$this->db->affected_rows() > 0 ? 
	redirect('funcoes?msg=1&type=2') : 
	redirect('funcoes?msg=2&type=2');}

public function dlt(){
	$where='id_funcao';
	$table='funcao';
	$id=$_POST['id_funcao'];	
	$this->load->model('crud');
	$this->crud->Delete($where,$id,$table);
	$this->db->affected_rows() > 0 ? 
	redirect('funcoes?msg=1&type=3') : 
	redirect('funcoes?msg=2&type=3');

}

}
?>