<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacoes extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_2']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'2');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		//Revela o id_setor do funcionario logado
		$where_user=array('id' => $id_login);
		$user=$this->crud->Select_where('usuarios',$where_user);
		$where_setor=array('id_funcionario' => $user[0]['funcionario']);
		$setor = $this->crud->Select_where('ficha',$where_setor);
		
		//Filtra as ordens de servico para mostrar as OS direcionadas aquele setor
		$dados_notificacao = array('id_setor_dest' => $setor[0]['id_setor']);
		$dados['notificacoes']=$this->crud->Select_where('comunicacao_interna',$dados_notificacao);
		
		//Filtra as ordens de servico para mostrar as OS aberta por aquele setor
		$dados_relacao = array('id_setor_emissor'=> $setor[0]['id_setor']);
		$dados['enviadas']=$this->crud->Select_where('comunicacao_interna',$dados_relacao);
		if(isset($access_level)&&$access_level>=1){
			$this->load->view('os/header_os',$dados);
			$this->load->view('os/notificacoes_view',$dados);
		}else{
			redirect('Selecao');
		}
	}
}
?>