<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cursos extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'6');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			/*-----Aqui deve ser inserida as arrays de dados que alimentar�o a view-----*/
			$dados['cursos']=$this->crud->Select_where_order('curso','ativo = 1','id_curso');
			/*-------------------------------------------------------------------------*/
			$this->load->view('alunos/header_alunos_view',$dados);
			$this->load->view('alunos/cursos_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function add(){	
		$dados=array('nome_curso' => strtoupper($_POST['nome']),
						'emec_curso' => $_POST['emec'],
						'ativo' => '1');
		$this->load->model('crud');
		$return = $this->crud->insert_checked('curso',$dados);
		//$this->db->affected_rows() > 0 ? 
		//redirect('funcoes?msg=1&type=1') : 
		//redirect('funcoes?msg=2&type=1');
		if($return['check']){
			redirect('cursos');	
		}
	}
		
	public function edt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		if(isset($access_level)&&$access_level>=1){
			$where='id_curso';
			$table='curso';
			$id=$_POST['id_curso'];
			
			$dados = array('nome_curso' => $_POST['nome_curso'],
							'emec_curso' => $_POST['emec_curso']);
			
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$dados);
			$this->db->affected_rows() == 1 ? 
				redirect('cursos') : //controle de mensagem de erro(entrou dados)
				redirect('cursos');//controle de mensagem de erro (n�o entrou dados)
		}
		//futuro else para erro de permis�o
	}
	
	public function dlt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		
		if(isset($access_level)&&$access_level==3){
			$where='id_curso';
			$table='curso';
			$id=$_POST['id_curso'];
			$data = array('ativo' => '0');
			
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$data);
			$this->db->affected_rows() == 1 ? 
				redirect('cursos') : //controle de mensagem de erro(entrou dados)
				die('error');//controle de mensagem de erro (n�o entrou dados)
			//redirect('empresa?msg=1&type=3') : 
			//redirect('empresa?msg=2&type=3');
		}
			else{
				redirect('cursos');
				//redirect('empresa?msg=fbd403&type=');
			}
	}
}
?>