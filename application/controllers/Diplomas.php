<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diplomas extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'6');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			/*-----Aqui deve ser inserida as arrays de dados que alimentarão a view-----*/
			$dados['diplomas']=$this->crud->Select('diplomas','nome_completo');
			/*-------------------------------------------------------------------------*/
			$this->load->view('alunos/header_alunos_view',$dados);
			$this->load->view('alunos/diplomas_view',$dados);
		}else{
			redirect('selecao');
		}
	}
}
?>