<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('usuarios');
		}else{
			redirect('selecao');
		}
	}

	public function usuarios(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('usuarios');
		}else{
			redirect('selecao');
		}
	}

	public function assuntos(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('assuntos');
		}else{
			redirect('selecao');
		}
	}

	public function sistemas(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('sistemas');
		}else{
			redirect('selecao');
		}
	}

	public function funcoes(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('funcoes');
		}else{
			redirect('selecao');
		}
	}

	public function setores(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('setores');
		}else{
			redirect('selecao');
		}
	}

	public function lixeira(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		if(isset($access_level)&&$access_level>=1){
			redirect('lixeira');
		}else{
			redirect('selecao');
		}
	}
}
?>