<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Grades extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_7']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'7');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			/*-----Aqui deve ser inserida as arrays de dados que alimentara a view-----*/
			$dados['cursos']=$this->crud->Select('curso','nome_curso');
			$dados['grades']=$this->crud->Select('grade_curricular','nome');	
			/*-------------------------------------------------------------------------*/
			$this->load->view('sys_acerto/header_acerto_view',$dados);
			$this->load->view('sys_acerto/grade_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function add(){
		$post_length = $_POST['nome_disc'];
		
		$dados_grade = array(
			'nome'=> $_POST['nome_grade'], 
			'id_curso'=> $_POST['curso'],
			'nivel_academico'=>  $_POST['nivel'],
			'data_publicacao_dou'=> $_POST['data_dou'],
			'portaria_normativa'=> $_POST['portaria']
		);
		$this->load->model('crud');
		$id_grade = $this->crud->insert('grade_curricular',$dados_grade);
		
		if($id_grade)
		{
			foreach($_POST['semestres'] as $semestre){
				$dados_sem  = array(
					'nome_sem'=> $semestre."º Semestre", 
					'carga_horaria_semestral'=> '',
					'id_grade_cur'=> $id_grade
				);
				$this->load->model('crud');
				$id_sem = $this->crud->insert('semestre',$dados_sem);
			}
			
			for($i=0;$i<count($post_length);$i++){
				$dados_disc  = array(
					'nome'=> $_POST["nome_disc"][$i][0], 
					'carga_horaria'=> $_POST["carga_horaria"][$i][0],
					'sem_indicado'=> $_POST["sem"][$i][0],
					'cod_tipo_disc'=> $_POST["tipo"][$i][0],
					'id_grade_cur'=> $id_grade
				);
				$this->load->model('crud');
				$ch_semestral = $this->crud
					->select_where('semestre',"nome_sem = '".$dados_disc['sem_indicado']."'");
			
				$ch_semestral = $ch_semestral[0]["carga_horaria_semestral"] 
								+ $dados_disc['carga_horaria'];
				
				$this->load->model('crud');
				$this->crud->update_where(	'semestre',
											array('carga_horaria_semestral' => $ch_semestral),
											"nome_sem = '".$dados_disc['sem_indicado']."'"
										);
				
				$this->load->model('crud');
				$id_disc = $this->crud->insert('disciplinas',$dados_disc);
			}
		}
		redirect('grades');
	}
}
?>