<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Info extends CI_Controller{
	
public function index(){
		$this->load->model('crud');
		$header=array('id_sistema'=>'4');
		$dados['header']=$this->crud->Select_where('sistemas',$header);

		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_4']);
		if(isset($access_level)&&$access_level>=1){
		$this->load->view('portaria/header_portaria_view',$dados);
		$this->load->view('portaria/info_view');
		}

		else{
			redirect('selecao');
		}
}
}
?>