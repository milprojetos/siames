<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sistemas extends CI_Controller{

	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'3');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="cadastrado";break;
					case 2:$action="editado";break;
					case 3:$action="ativado";break;
					case 4:$action="desativado";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Sistema '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Sistema não '.$action.', ocorreu um <strong>erro</strong>.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}
			$this->load->model('crud');
			$ativo=array('1' => '1');
			$dados['select']=$this->crud->Select_where_order('sistemas',$ativo,'id_sistema');
			$this->load->view('admin/header_adm_view',$dados);
			$this->load->view('admin/sistemas_view',$dados);
		}else{
			redirect('selecao');
		}
	}

	public function add(){
		if(file_exists($_FILES['path']['tmp_name'])){
		$explode=explode('.',$_FILES['path']['name']);
		$path=$explode[0];
		}
		else{
			$path='#';
		}
		$dados = array(	'nome' => $_POST['nome'],
						'descricao' => $_POST['descricao'],
						'system_method' => $path,
						'ativo'=>'0'
						);
		$this->load->model('crud');
		$this->crud->insert('sistemas',$dados);
		$this->db->affected_rows() > 0 ? 
		redirect('sistemas?msg=1&type=1') : 
		redirect('sistemas?msg=2&type=1');
	}

	public function edt(){
		if(file_exists($_FILES['path']['tmp_name'])){
			$explode=explode('.',$_FILES['path']['name']);
			$path=$explode[0];
		}
		else{
			$path=$_POST['path_velho'];
		}
		$where='id_sistema';
		$table='sistemas';
		$id=$_POST['id_sistema'];
		$dados = array(	'nome' => $_POST['nome'],
						'descricao' => $_POST['descricao'],
						'system_method' => $path);
		
		$this->load->model('crud');
		$this->crud->Update($where,$id,$table,$dados);
		$this->db->affected_rows() > 0 ? 
		redirect('sistemas?msg=1&type=2') : 
		redirect('sistemas?msg=2&type=2');
	}
	
	public function toogle_sys(){
		$new_status=$_POST['new_status'];
		$system_id=$_POST['system_id'];
		
		$dados = array('id_sistema'=>$system_id, 'ativo'=>$new_status);
		
		$this->load->model('crud');
		$this->crud->Update_where('sistemas',$dados,'id_sistema = '.$system_id);
	}
}
?>