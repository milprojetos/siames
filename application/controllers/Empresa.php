<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empresa extends CI_Controller{

	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_1']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'1');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="cadastrada";break;
					case 2:$action="editada";break;
					case 3:$action="excluida";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Empresa '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Empresa não '.$action.', ocorreu um <strong>erro</strong>.';
					break;
					
					case 'fbd403':
						$dados['alert_type']='danger';
						$dados['mensagem']='<strong>Erro!</strong> Você não possui permissão para utilizar essa função.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}


			$this->load->model('crud');
			$ativo=array('ativo' => '1');
			$dados['select']=$this->crud->Select_where_order('empresa',$ativo,'id_empresa');
			$this->load->view('sys_ponto/dashboard_view',$dados);
			$this->load->view('sys_ponto/empresa_view',$dados);
			$this->load->view('sys_ponto/footer');
		}else{
			redirect('selecao');
		}
	}
	
	public function add(){
	$id_login = $_SESSION['id'];
	$access_level = isset($_SESSION['sys_1']);
	if(isset($access_level)&&$access_level>=1){
	$_POST= array_map('strtoupper', $_POST);
		$dados = array(	'razao_social' => $_POST['empregador'],
						'cnpj' => $_POST['cnpj'],
						'atividade_economica' => $_POST['atividade'],
						'endereco' => $_POST['endereco'],
						'ativo'=>'1'
						);
		$this->load->model('crud');
		$this->crud->insert('empresa',$dados);
		$this->db->affected_rows() > 0 ? 
		redirect('empresa?msg=1&type=1') : 
		redirect('empresa?msg=2&type=1');
	}
		else{
			redirect('empresa?msg=fbd403&type=');
		}
	}

	public function edt(){
	$id_login = $_SESSION['id'];
	$access_level = isset($_SESSION['sys_1']);
	if(isset($access_level)&&$access_level>=1){
		$where='id_empresa';
		$table='empresa';
		$id=$_POST['id_empresa'];
		$dados = array('razao_social' => $_POST['empregador'],'cnpj' => $_POST['cnpj'],'atividade_economica' => $_POST['atividade'], 'endereco' => $_POST['endereco']);
		
		$this->load->model('crud');
		$this->crud->Update($where,$id,$table,$dados);
		$this->db->affected_rows() > 0 ? 
		redirect('empresa?msg=1&type=2') : 
		redirect('empresa?msg=2&type=2');


	}
		else{
			redirect('empresa?msg=fbd403&type=');
		}
	}

	public function dlt(){
	$id_login = $_SESSION['id'];
	$access_level = isset($_SESSION['sys_1']);
	if(isset($access_level)&&$access_level==3){
		$where='id_empresa';
		$table='empresa';
		$id=$_POST['id_empresa'];
		$data = array('ativo' => '0');
		
		$this->load->model('crud');
		$this->crud->Update($where,$id,$table,$data);
		$this->db->affected_rows() > 0 ? 
		redirect('empresa?msg=1&type=3') : 
		redirect('empresa?msg=2&type=3');
	}
		else{
			redirect('empresa?msg=fbd403&type=');
		}
	}
}
?>