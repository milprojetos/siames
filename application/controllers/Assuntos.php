<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assuntos extends CI_Controller{
	
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_3']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'3');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		if(isset($access_level)&&$access_level>=1){
			//Alerts
			//----------------------------------------------------------//
			//Converter para string
			parse_str(substr(strrchr($_SERVER['REQUEST_URI'], "?"), 1), $_GET);
			//Default
			$dados['mensagem']=' ';
			//Se tiver sido passada a variável msg
			if((isset($_GET['msg'])&&(isset($_GET['type'])))){
				$msg=$_GET['msg'];
				$type=$_GET['type'];
				//Type vai definir a ação que foi feita.
				switch($type)
				{
					case 1:$action="cadastrado";break;
					case 2:$action="editado";break;
					case 3:$action="excluido";break;
					default:$action="";break;
				}
				//Msg vai definir se foi um sucesso ou ocorreu um erro. Responsável pela cor do Alert
				switch ($msg) {
					case 1:
						$dados['alert_type']='success';
						$dados['mensagem']='Assunto '.$action.' com <strong>sucesso</strong>.';
					break;

					case 2:
						$dados['alert_type']='danger';
						$dados['mensagem']='Assunto não '.$action.', ocorreu um <strong>erro</strong>.';
					break;
					
					default:
						$dados['alert_type']='warning';
						$dados['mensagem']=' ';
						break;
				}
			}

			$this->load->model('crud');
			$ativo=array('1' => '1');
			$dados['select']=$this->crud->Select_where_order('assunto',$ativo,'id_assunto');
			$dados['setor']=$this->crud->Select_where_order('setores',$ativo,'id_setor');
			$dados['as']=$this->crud->Select_where_order('assuntos_setores',$ativo,'id_setor');
			$this->load->view('admin/header_adm_view',$dados);
			$this->load->view('admin/assuntos_view',$dados);
		}else{
			redirect('selecao');
		}
	}

	public function add(){	
		$dados=array(
					'assunto'=>$_POST['assunto'],
					'descricao'=>$_POST['descricao']
					);
		$this->load->model('crud');
		$last = $this->crud->insert('assunto',$dados);

		//Para cada checkbox
		foreach ($this->input->post('setor') as $setor) {
			$as = array('id_assunto' =>  $last,
							'id_setor' => $setor,
						);
			$this->crud->insert('assuntos_setores',$as);
			}

			$this->db->affected_rows() > 0 ? 
			redirect('assuntos?msg=1&type=1') : 
			redirect('assuntos?msg=2&type=1');
	}

	public function edt(){
		$where='id_assunto';
		$table='assunto';
		$id=$_POST['id_assunto'];
		$dados=array(
					'assunto'=>$_POST['assunto'],
					'descricao'=>$_POST['descricao']
					);
		$this->load->model('crud');
		$this->crud->Update($where,$id,$table,$dados);
		$this->db->affected_rows() > 0 ? 
		redirect('assuntos?msg=1&type=2') : 
		redirect('assuntos?msg=2&type=2');
	}

	public function dlt(){
		$where='id_assunto';
		$table='assunto';
		$id=$_POST['id_assunto'];	
		$this->load->model('crud');
		$this->crud->Delete($where,$id,$table);
		$this->db->affected_rows() > 0 ? 
		redirect('assuntos?msg=1&type=3') : 
		redirect('assuntos?msg=2&type=3');

	}
}
?>