<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aluno extends CI_Controller{
	public function index(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		$this->load->model('crud');
		$header=array('id_sistema'=>'6');
		$dados['header']=$this->crud->Select_where('sistemas',$header);
		
		if(isset($access_level)&&$access_level>=1){
			$this->load->model('crud');
			/*-----Aqui deve ser inserida as arrays de dados que alimentar�o a view-----*/
			$dados['alunos']=$this->crud->Select('alunos_matriculados','id_matricula');
			$dados['ies']=$this->crud->Select_where_order('ies','ativo >= 0','id_ies');
			$dados['cursos']=$this->crud->Select_where_order('curso','ativo >= 0','id_curso');	
			/*-------------------------------------------------------------------------*/
			$this->load->view('alunos/header_alunos_view',$dados);
			$this->load->view('alunos/alunos_view',$dados);
		}else{
			redirect('selecao');
		}
	}
	
	public function add(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		if(isset($access_level)&&$access_level>=1){

			$cpf_parcial = substr($_POST['cpf'],0,6);
			//Captura o $_POST e o converte para mai�sculo
			$_POST = array_map('strtoupper', $_POST);
			//Back-End da tabela de Alunos
			$data_alunos = array(
							'nome_completo' => $_POST['nome'],
							'cpf' => $_POST['cpf'],
							'cpf_parcial' => $cpf_parcial
							);
							
			$this->load->model('crud');
			$last_aluno = $this->crud->Insert_checked('alunos',$data_alunos);
			//Back-End da tabela de Certificados
			$data_certificados = array(
							'data_conclusao' => $_POST['data_out'],
							'data_exp_diploma' => $_POST['data_exp_dip'],
							'data_reg_diploma' => $_POST['data_reg_dip'],
							'cod_controle_exp' => $_POST['num_exp'],
							'cod_controle_reg' => $_POST['num_reg'],
							'data_publicacao_dou' => $_POST['data_pub'],
							'id_ies_expedidora' => $_POST['ies'],
							'id_ies_registradora ' => $_POST['ies_reg']
							);
							
			$this->load->model('crud');
			$last_certificado = $this->crud->Insert_checked('certificado',$data_certificados);
			
			if(!$last_certificado['check']){
				$this->load->model('crud');
				$return = $this->crud->delete('id_aluno',$last_aluno['last_id'],'alunos');
			}
			else{
				//Back-End da tabela de Matriculas
				$data_matriculas = array(
									'id_curso' => $_POST['curso'],
									'num_matricula' => $_POST['ra'],
									'id_aluno' => $last_aluno['last_id'],
									'data_ingresso' => $_POST['data_in'],
									'id_certificado' => $last_certificado['last_id'],
				);
				
				$this->load->model('crud');
				$last_matricula = $this->crud->Insert_checked('matricula',$data_matriculas);
				
				if(!$last_matricula['check']){
					$this->load->model('crud');
					$aluno_return = $this->crud->delete('id_aluno',
															$last_aluno['last_id'],
															'alunos'
															);
															
					$this->load->model('crud');
					$certificado_return = $this->crud->delete('id_certificado',
																$last_certificado['last_id'],
																'certificado'
																);
				}
				else{
					redirect('aluno');
				}
				redirect('aluno');
			}
			
		}
	}
	
	public function edt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		if(isset($access_level)&&$access_level>=1){
			//Captura o $_POST e o converte para mai�sculo
			$_POST = array_map('strtoupper', $_POST);
			
			//Back-End da tabela de Matriculas
			$where='id_matricula';
			$table='matricula';
			$id=$_POST['id_matricula'];
			
			$this->load->model('crud');
			$matricula_aluno = $this->crud->Select_where_order($table,
																'id_matricula = '.$id,
																'id_matricula');
			$data_matriculas = array(
								'id_curso' => $_POST['curso'],
								'num_matricula' => $_POST['ra'],
								'id_aluno' => $matricula_aluno[0]['id_aluno'],
								'data_ingresso' => $_POST['data_in'],
								'id_certificado' => $matricula_aluno[0]['id_certificado'],
								);
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$data_matriculas); 
			
			//Back-End da tabela de Aluno
			$where = 'id_aluno';
			$table = 'alunos';
			$id = $matricula_aluno[0]['id_aluno']; 
			$cpf_parcial = substr($_POST['cpf'],0,6);
			
			$data_alunos = array(
						'nome_completo' => $_POST['nome'],
						'cpf' => $_POST['cpf'],
						'cpf_parcial' => $cpf_parcial
						);
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$data_alunos);
			
			//Back-End da tabela de Certificado
			$where = 'id_certificado';
			$table = 'certificado';
			$id = $matricula_aluno[0]['id_certificado']; 
			
			$data_certificados = array(
									'data_conclusao' => $_POST['data_out'],
									'data_exp_diploma' => $_POST['data_exp_dip'],
									'data_reg_diploma' => $_POST['data_reg_dip'],
									'cod_controle_exp' => $_POST['num_exp'],
									'cod_controle_reg' => $_POST['num_reg'],
									'data_publicacao_dou' => $_POST['data_pub'],
									'id_ies_expedidora' => $_POST['ies'],
									'id_ies_registradora ' => $_POST['ies_reg']
									);
			$this->load->model('crud');
			$this->crud->Update($where,$id,$table,$data_certificados);
			redirect('aluno');
		}
		//futuro else para erro de permis�o
	}
	
	public function dlt(){
		$id_login = $_SESSION['id'];
		$access_level = isset($_SESSION['sys_6']);
		if(isset($access_level)&&$access_level>=1){
			//Captura o $_POST e o converte para mai�sculo
			$_POST = array_map('strtoupper', $_POST);
			
			//Back-End da tabela de Matriculas
			$where='id_matricula';
			$table='matricula';
			$id=$_POST['id_matricula'];
			
			$this->load->model('crud');
			$matricula_aluno = $this->crud->Select_where_order($table,
																'id_matricula = '.$id,
																'id_matricula');
			$this->load->model('crud');
			$this->crud->Delete($where,$id,$table); 
			
			//Back-End da tabela de Aluno
			$where = 'id_aluno';
			$table = 'alunos';
			$id = $matricula_aluno[0]['id_aluno']; 
			
			$this->load->model('crud');
			$this->crud->Delete($where,$id,$table); 
			
			//Back-End da tabela de Certificado
			$where = 'id_certificado';
			$table = 'certificado';
			$id = $matricula_aluno[0]['id_certificado']; 
			
			$this->load->model('crud');
			$this->crud->Delete($where,$id,$table); 
			redirect('aluno');
		}
		//futuro else para erro de permis�o
	}
}
?>