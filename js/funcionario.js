$(document).ready(function(){
	//Insere um input vazio para a inserção de novos emails no modal de Insert
	$('.new_email').click(function(){
		$(this).prev().append(
			'<label for="email">E-mail Secundario:</label><input type="email"'+ 
			'class="form-control" name="email[]">'
		);
	});

	// Insere um input vazio para a inserção de novos emails no modal de update
	$('.add_email').click(function(){
		$('.emails_bunker').append('<div class="ghost_div"><div class="input-group mb-3">'+
			'<input type="email" class="form-control down" name="email[]" required>'+
			'<div class="input-group-append"><button type="button"'+
			'class="btn btn-outline-danger delete_email_container"><i class="fas fa-trash-alt"></i>'+
			'</button></div></div></div>'
		);
	});

	// Limpa todos os inputs de email não inseridos no DB ao abrir um novo modal de update
	$('.edit_button').click(function(){
		$('.emails_bunker').html('');	
	});

	//Apaga os inputs vazios que seriam usados para inserção de novos emails no modal de update
	$(document).on('click','.delete_email_container',function(){
		$(this).parent().parent().parent().html('');
	});

	//Ajax para eclusão de email secundao já cadastrado
	$('.delete_email').click(function(){
		var id_email = $(this).prev('.id_email').val();
		$(this).parent().parent().parent().addClass('to_erase');
		
		$.post(dlt_email,{id : id_email} ,function( data ) {
			if(data=='true'){
				$('.to_erase').html('');
			}
			else{
				$('.to_erase').removeClass('to_erase');
				alert('Ocorreu um erro na exclusão do email');
			}
		});
	});
});