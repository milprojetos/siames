//Captura a data
var hj=new Date();
var mesano=hj.getFullYear()+"-"+(('0'+(hj.getMonth()+1)).slice(-2));
var mes = document.getElementById('mes');
mes.value = mesano;

$(document).ready(function(){
  $( '.marcatudo' ).click( function () {
    $( 'input[type="checkbox"]' ).prop('checked', this.checked);
  });
  
  //Desabilita o botão
  $("button[type=submit]").attr("disabled", "disabled");
  
  $('input[type=checkbox]').change(function(){
    // Se for verdadeiro, o botão é habilitado
    if($('input[type=checkbox]').is(":checked")) { 
      //Remove desabilitação se qualquer um for checado
      $("button[type=submit]").removeAttr("disabled");
      }
      else{
        if (!$('input[type=checkbox]').is(':checked')) {
          //Adiciona quando TODOS estiverem não checados
          $("button[type=submit]").attr("disabled", "disabled");
        }
      }
    });
  $('input:first').trigger('change');
});