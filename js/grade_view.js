$(document).ready(function(){
	var cont = 0;
	var cont_disc = 0;
	
	//Insere um input vazio para a inserção de novos Semestres no modal de Insert
	$('.new_sem').click(function(){
		cont++;
		$(this).parents('.form-group').find('.sem_inputs').append(
			'<div class="tab-pane fade sem_'+cont+'" id="nav-sem-'+cont+'"'+
			'role="tabpanel" aria-labelledby="nav-'+cont+'-tab" >'+
				'<b><label class="pt-2" for="semestre">'+cont+'º Semestre:</label></b>'+
				'<input type="hidden" name="semestres[]" value="'+cont+'">'+
				'<div>'+							
					'<table class=" table table-sm table-striped">'+
						'<thead>'+
							'<th>Disciplinas</th>'+
							'<th>Carga Horaria</th>'+
							'<th>Semestre</th>'+
							'<th>Tipo</th>'+
							'<th>Grade Curricular</th>'+
							'<th>Ações</th>'+
						'</thead>'+
						
						'<tbody class="disc_inputs">'+
							'<input type="hidden" id="sem_indicado" name="sem_indicado" value="'+cont+'">'+
						'</tbody>'+
					'</table>'+
				'</div>'+
				'<hr>'+
				'<div class="d-flex justify-content-center" style="font-size: 0.8rem;">'+
					'<i class="fas fa-plus-circle new_disc fa-2x"></i>'+
				'</div>'+
			'</div>'
		);
		
		$(this).parents('.form-group').find('.nav_sem').append(
			'<a class="nav-item nav-link" id="nav-'+cont+'-tab" data-toggle="tab" href="#nav-sem-'+cont+ 
			'" role="tab" aria-controls="#nav-sem-'+cont+'" aria-selected="false">'+cont+'º Sem</a>'
		);		
		
		if(cont==1){
			$(this).parents('.form-group').find('#nav-'+cont+'-tab').addClass('active');
			$(this).parents('.form-group').find('#nav-sem-'+cont).addClass('show active');
		}
	});
	
	$(document).on('click','.new_disc',function(){
		var sem_indicado = $(this).parents('.tab-pane').find('#sem_indicado').val();
		var grade_curricular = $(this).parents('div').find('.nome_grade_cad').val();
		$(this).parents('.tab-pane').find('.disc_inputs').append(
			'<tr>'+
				'<td>'+
					'<input type="text" class="form-control-plaintext" id="nome_disc" name="nome_disc['+cont_disc+'][]"'+
					'placeholder="Nome da Disciplina">'+	
				'</td>'+
				
				'<td>'+
					'<input type="number"  min="0" max = "999" class="form-control-plaintext"'+
					'id="carga_horaria" name="carga_horaria['+cont_disc+'][]" placeholder="0" '+
					'step= "40">'+	
				'</td>'+
				
				'<td>'+
					'<input type="text" class="form-control-plaintext" id="sem" '+
					'name="sem['+cont_disc+'][]" readonly="readonly" '+
					'placeholder="'+sem_indicado+'º Semestre" '+
					'value="'+sem_indicado+'º Semestre">'+
				'</td>'+
				
				'<td>'+
					'<select class="form-control-plaintext" id="tipo" '+
					'name="tipo['+cont_disc+'][]" required>'+
						'<option value="1">'+
							'Obrigatoria'+
						'</option>'+
						
						'<option value="2">'+
							'Opcional'+
						'</option>'+
						
						'<option value="3">'+
							'Atividades Complementares'+
						'</option>'+
					'</select>'+
				'</td>'+
				
				'<td>'+
					'<input type="text" class="form-control-plaintext grade_cad" '+
					'id="grade" name="grade['+cont_disc+'][]"'+
					'readonly="readonly" value="'+grade_curricular+'">'+
				'</td>'+
				'<td>'+
					'<button type="button" class="btn btn-sm excluir_disc"'+
					'style="font-size: 0.8rem;">'+
						'<i class="fas fa-minus-circle excluir_disc fa-2x"></i>'+
					'</button>'+
				'</td>'+
			'</tr>'
		);
		cont_disc++;
	});
	
	$(document).on('click','.excluir_disc',function(){
		$(this).parents('tr').remove();
	});
	
	$(".nome_grade_cad").on("change paste keyup", function() {
		var grade_val = $(this).val();
		$(document).find('.grade_cad').val(grade_val);
	});
	
	$(document).on('click','.dlt_sem',function(){
		if($(this).parents('.form-group').find('#nav-'+cont+'-tab').hasClass('active')){
			var prev_cont = cont - 1;
			$(this).parents('.form-group').find('#nav-'+prev_cont+'-tab').addClass('active');
			$(this).parents('.form-group').find('#nav-sem-'+prev_cont).addClass('show active');
		}
		$(this).parents('.form-group').find('div.sem_'+cont).remove();
		$(this).parents('.form-group').find('#nav-'+cont+'-tab').remove();
		if(cont>0){cont--}
	});
});