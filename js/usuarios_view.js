$(document).ready(function(){
	var global_id_login;
	$('.insert_access').click(function(){
		var access_level = $(this).closest('.first_div').find('.nivel option:selected').val();
		var method_address = '';
		var id_sistema = $(this).val();
		var id_login = $(this).closest('.form-group').find('.id_login').val();
		
		if(this.checked){
			method_address = insert_access;
		}else{
			method_address = remove_access;
		}
		
		$.ajax({
			type: 'POST',
			url: method_address,
			data: {id_sys : id_sistema, id_log : id_login, level_val : access_level},
			success: function( data ){
			},
			async:false
		});
	});
	
	$('.edit_access_modal').on('show.bs.modal', function () {
		var id_login = $(this).find('.id_login').val();
		global_id_login = id_login;
	});
	
	$( ".nivel" ).change(function() {
		var access_level = this.value;
		var id_sistema = $(this).closest('.first_div').find('.insert_access').val();
		var id_login = global_id_login;
		
		if($(this).closest('.first_div').find('.insert_access').is(':checked')){
			$.ajax({
				type: 'POST',
				url: update_access,
				data: {id_sys : id_sistema, id_log : id_login, level_val : access_level},
				success: function( data ){
				},
				async:false
			});
		}	
	});
});