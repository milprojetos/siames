$(document).ready(function(){
	$('#assunto').val(''); 
	$(".setor_destino").val('');
	$('.msg').hide();
	$('.fechar').hide();
	
	//Captura a data
	var hj=new Date();
	var mesano=hj.getFullYear()+"-"+
	('0'+(hj.getMonth()+1)).slice(-2)+"-"+
	('0'+hj.getDate()).slice(-2);
	var mes = document.getElementById('data');
	mes.value = mesano;
	 
	$( ".setor_destino" ).change(function() {
		var id_setor = $(this).val();
		
		//retorna o select de assuntos para default quand o setor for trocado
		$('#assunto').val(''); 
		$('#assunto').trigger('change');
		//esconde todos os assuntos de select
		$('.assuntos').hide();
		//mostra no select apenas aqueles vinculados ao novo select
		$('.'+id_setor).show();
	});
	
	//Mostra e adiciona Submit
	$('.env').click(function(){
		if($(this).hasClass('.submit')){
			var msg = $(this).parent().find('.msg').val();
			var id_emissor = $(this).parent().find('.id_solicitador').val();
			var id_solicitacao = $(this).parent().find('.id_requisicao').val();
			var id_setor_destino = $(this).parent().find('.id_setor_dest').val();
			var date = $(this).parent().find('.response_date').val();
			var nivel = 0;
			var id_assunto = 0;
			
			$.post(adicionar_os,
			{
				descricao : msg, 
				id : id_emissor, 
				prioridade : nivel,
				assunto : id_assunto,
				setor_destino : id_setor_destino,
				data : date
			});
		}else{
			$(this).parent().find('.msg').show();
			$(this).parent().find('.fechar').show();	
			$(this).addClass( ".submit" );
		}
	});

	//Oculta e remove Submit
	$('.fechar').click(function(){
		$(this).parent().find('.msg').hide();
		$(this).parent().find('.fechar').hide();
		$(this).parent().find('.env').removeClass( ".submit" );
	});
});