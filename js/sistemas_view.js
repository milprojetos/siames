$(document).ready(function(){
	$('.toogle_sys').click(function(){
		var system_status;
		var id_sistema = $(this).closest('.toogle_block').find('.hidden_id').val();
		
		if($(this).hasClass('fa-toggle-on')){
			$(this).removeClass('fa-toggle-on').addClass('fa-toggle-off');
			system_status = 0;
		}else if($(this).hasClass('fa-toggle-off')){
			$(this).removeClass('fa-toggle-off').addClass('fa-toggle-on');
			system_status = 1;
		}

		$.ajax({
			type: 'POST',
			url: toogle_sys,
			data: {system_id : id_sistema, new_status : system_status},
			success: function( data ){
			},
			async:false
		});
	})
});