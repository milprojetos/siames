using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace Fif_siames
{
    #region Disciplinas
    public class Disciplinas
    {
        #region Member Variables
        protected int _id_disc;
        protected string _nome;
        protected int _carga_horaria;
        protected int _sem_indicado;
        protected int _tipo_disc;
        protected int _id_grade_cur;
        #endregion
        #region Constructors
        public Disciplinas() { }
        public Disciplinas(string nome, int carga_horaria, int sem_indicado, int tipo_disc, int id_grade_cur)
        {
            this._nome=nome;
            this._carga_horaria=carga_horaria;
            this._sem_indicado=sem_indicado;
            this._tipo_disc=tipo_disc;
            this._id_grade_cur=id_grade_cur;
        }
        #endregion
        #region Public Properties
        public virtual int Id_disc
        {
            get {return _id_disc;}
            set {_id_disc=value;}
        }
        public virtual string Nome
        {
            get {return _nome;}
            set {_nome=value;}
        }
        public virtual int Carga_horaria
        {
            get {return _carga_horaria;}
            set {_carga_horaria=value;}
        }
        public virtual int Sem_indicado
        {
            get {return _sem_indicado;}
            set {_sem_indicado=value;}
        }
        public virtual int Tipo_disc
        {
            get {return _tipo_disc;}
            set {_tipo_disc=value;}
        }
        public virtual int Id_grade_cur
        {
            get {return _id_grade_cur;}
            set {_id_grade_cur=value;}
        }
        #endregion
    }
    #endregion
}